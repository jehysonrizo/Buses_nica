import 'package:flutter/material.dart';

const kDefaultPaddin = 20.0;
const jAnimationDuration = Duration(milliseconds: 200);
const jSecondaryColor = Color(0xFF979797);
const jTextColor = Color(0xFF757575);
const kTextColor = Color(0xFF535353);
const jTextLightColor = Color(0xFFACACAC);
const depColorJin = Color.fromARGB(255, 14, 150, 143);
const Color cafeBrown = Color.fromARGB(255, 112, 82, 27);
const Color backGroundColorApp = Color(0xff2E305F);
const Color backGroundColorScaff = Color(0xff2E305F);
Color c1 = const Color(0xFFBADCE5);

ThemeData theme() {
  return ThemeData(
    appBarTheme: appBarTheme(),
    fontFamily: "Muli",
    textTheme: textTheme(),
    inputDecorationTheme: imputDecorationTheme(),
    visualDensity: VisualDensity.adaptivePlatformDensity,
    primarySwatch: Colors.blue,
  );
}

InputDecorationTheme imputDecorationTheme() {
  OutlineInputBorder outlineInputBorder = OutlineInputBorder(
      borderRadius: BorderRadius.circular(18),
      borderSide: const BorderSide(color: jTextColor),
      gapPadding: 10);
  return InputDecorationTheme(
      //floatingLabelBehavior: FloatingLabelBehavior.always,
      contentPadding: const EdgeInsets.symmetric(horizontal: 42, vertical: 20),
      enabledBorder: outlineInputBorder,
      focusedBorder: outlineInputBorder,
      border: outlineInputBorder);
}

TextTheme textTheme() {
  return const TextTheme(
    // ignore: deprecated_member_use
    bodyText1: TextStyle(color: jTextColor),
    // ignore: deprecated_member_use
    bodyText2: TextStyle(color: jTextColor),
  );
}

AppBarTheme appBarTheme() {
  return const AppBarTheme(
      color: Color(0xff2E305F),
      elevation: 0,
      // ignore: deprecated_member_use
      brightness: Brightness.light,
      iconTheme: IconThemeData(color: Colors.black),
      // ignore: deprecated_member_use
      textTheme: TextTheme(
          // ignore: deprecated_member_use
          headline6: TextStyle(color: Color(0XFF8B8B8B), fontSize: 18)));
}
