import 'dart:math';

import 'package:flutter/material.dart';

class BackGround extends StatelessWidget {
  final boxDecoration = const BoxDecoration(
      gradient: LinearGradient(
          stops: [0.2, 0.9],
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [Color(0xff2E305F), Color(0xff202333)]));
  const BackGround({super.key});

  @override
  Widget build(BuildContext context) {
    return Stack(children: [
      Container(
        //purple
        decoration: boxDecoration,
        //pinkbox
      ),
      const Positioned(
        top: -150,
        left: -30,
        child: PinkBox()),
         const Positioned(
        top: 600,
        left: 200,
        child: PinkBox())
    ]);
  }
}

class PinkBox extends StatelessWidget {
  const PinkBox({super.key});

  @override
  Widget build(BuildContext context) {
    // ignore: prefer_typing_uninitialized_variables
    return Transform.rotate(
      angle: -pi / 5,
      child: Container(
        width: 360,
        height: 360,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(80),
            gradient: const LinearGradient(colors: [
              Color.fromRGBO(134, 105, 60, 1),
              Color.fromRGBO(212, 167, 42, 0.8),
            ])),
      ),
    );
  }
}
