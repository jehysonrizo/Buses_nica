// ignore_for_file: file_names
import 'package:buses_nica/constants.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

import 'Button_part_splash.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen>
    with TickerProviderStateMixin {
  late AnimationController _busController;
  bool copAnimated = false;
  bool animateBusText = false;

  @override
  void initState() {
    super.initState();
    _busController = AnimationController(vsync: this);
    _busController.addListener(() {
      if (_busController.value > 0.7) {
        _busController.stop();
        copAnimated = true;
        setState(() {});
        Future.delayed(const Duration(seconds: 1), () {
          animateBusText = true;
          setState(() {});
        });
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
    _busController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var screenHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: cafeBrown,
      body: Stack(
        children: [
          // White Container top half
          AnimatedContainer(
            duration: const Duration(seconds: 1),
            height: copAnimated ? screenHeight / 1.9 : screenHeight,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.vertical(
                  bottom: Radius.circular(copAnimated ? 40.0 : 0.0)),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Visibility(
                  visible: !copAnimated,
                  child: Lottie.asset(
                    'assets/bus.json',
                    controller: _busController,
                    onLoaded: (composition) {
                      _busController
                        ..duration = composition.duration
                        ..forward();
                    },
                  ),
                ),
                Visibility(
                  visible: copAnimated,
                  child: Image.asset(
                    'assets/bus.png',
                    height: 190.0,
                    width: 190.0,
                  ),
                ),
                Center(
                  child: AnimatedOpacity(
                    opacity: animateBusText ? 1 : 0,
                    duration: const Duration(seconds: 1),
                    child: const Text(
                      'BUSES NICA',
                      style: TextStyle(fontSize: 50.0, color: Colors.brown),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Visibility(visible: copAnimated, child: const BottomPart()),
        ],
      ),
    );
  }
}
