import 'package:buses_nica/constants.dart';
import 'package:flutter/material.dart';

class ShowDialog extends StatefulWidget {
  const ShowDialog({super.key});

  @override
  State<ShowDialog> createState() => _ShowDialogState();
}

class _ShowDialogState extends State<ShowDialog> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        backgroundColor: c1,
        appBar: AppBar(
            title: const Text(
              'Opssss!',
            ),
            centerTitle: true,
            backgroundColor: backGroundColorApp,
            leading: IconButton(
              onPressed: () {
                Navigator.pop(context);
              },
              icon: const Icon(
                Icons.arrow_back,
                color: Colors.white,
              ),
            )),
        body: const Center(
          child: FadeInImage(
            image: AssetImage('assets/no page2.png'),
            placeholder: AssetImage('assets/autobus.gif'),
            fadeInDuration: Duration(milliseconds: 800),
            fit: BoxFit.cover,
          ),
        ),
      ),
    );
  }
}
