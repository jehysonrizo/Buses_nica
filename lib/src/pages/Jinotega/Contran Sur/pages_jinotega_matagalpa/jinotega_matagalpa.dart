import 'package:buses_nica/constants.dart';
import 'package:buses_nica/src/pages/Jinotega/Contran%20Sur/pages_jinotega_matagalpa/component/body_jinotega.dart';
import 'package:buses_nica/src/views/background.dart';
import 'package:flutter/material.dart';

class JinotegaMata extends StatelessWidget {
  const JinotegaMata({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: const Text('Jinotega - Contran Sur'),
          backgroundColor: backGroundColorApp,
          centerTitle: true,
          automaticallyImplyLeading: false,
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: const Icon(
              Icons.arrow_back,
              color: Colors.white,
            ),
          )),
      body: Stack(children: const [BackGround(), Body()]),
    );
  }
}
