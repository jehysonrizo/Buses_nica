import 'package:buses_nica/models_departament/Jinotega_Models/Model_Sur/Jinotega_Sur_Matagalpa.dart';
import 'package:flutter/material.dart';

class ItemCard extends StatefulWidget {
  final JinotegaMatagalpa matagalpa;
  // ignore: use_key_in_widget_constructors
  const ItemCard({
    required this.matagalpa,
  });

  @override
  State<ItemCard> createState() => _ItemCardState();
}

class _ItemCardState extends State<ItemCard> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(child: card());
  }

  Widget card() {
    return Padding(
      padding: const EdgeInsets.only(top: 8),
      child: Card(
        elevation: 8.0,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
        child: Column(
          children: [
            ListTile(
              title: Text(
                widget.matagalpa.title,
                style: const TextStyle(fontWeight: FontWeight.w700),
              ),
              subtitle: Text(widget.matagalpa.subtitle),
              trailing: Text(widget.matagalpa.transporte,
                  style: const TextStyle(
                      fontWeight: FontWeight.w700, color: Colors.indigo)),
            ),
            const FadeInImage(
              image: AssetImage('assets/bus.png'),
              placeholder: AssetImage('assets/loading.gif'),
              fadeInDuration: Duration(milliseconds: 200),
              height: 180,
              fit: BoxFit.cover,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  widget.matagalpa.entrada1,
                  style: const TextStyle(fontWeight: FontWeight.w600),
                ),
                Text(widget.matagalpa.entrada2,
                    style: const TextStyle(fontWeight: FontWeight.w600)),
                Text(widget.matagalpa.entrada3,
                    style: const TextStyle(fontWeight: FontWeight.w600)),
                Text(widget.matagalpa.entrada4,
                    style: const TextStyle(fontWeight: FontWeight.w600)),
                const SizedBox(
                  width: 300,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
