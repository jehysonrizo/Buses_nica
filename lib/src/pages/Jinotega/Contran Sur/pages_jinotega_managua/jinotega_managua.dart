import 'package:buses_nica/constants.dart';
import 'package:buses_nica/src/pages/Jinotega/Contran%20Sur/pages_jinotega_managua/component/body_jinotega.dart';
import 'package:flutter/material.dart';

import '../../../../views/background.dart';

class JinotegaMana extends StatelessWidget {
  const JinotegaMana({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backGroundColorScaff,
      appBar: AppBar(
          title: const Text('Jinotega - Contran Sur'),
          backgroundColor: backGroundColorApp,
          centerTitle: true,
          automaticallyImplyLeading: false,
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: const Icon(
              Icons.arrow_back,
              color: Colors.white,
            ),
          )),
      body: Stack(children: const [BackGround(),Body()]),
    );
  }
}
