import 'package:buses_nica/constants.dart';
import 'package:buses_nica/src/pages/Matagalpa/Cotran_Sur/pages_matagalpa_leon/component/body_matagalpa.dart';
import 'package:buses_nica/src/views/background.dart';

import 'package:flutter/material.dart';

class MatagalpaLeo extends StatelessWidget {
  const MatagalpaLeo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backGroundColorScaff,
      appBar: AppBar(
          title: const Text('Matagalpa - Contran Sur'),
          backgroundColor: backGroundColorApp,
          centerTitle: true,
          automaticallyImplyLeading: false,
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: const Icon(
              Icons.arrow_back,
              color: Colors.white,
            ),
          )),
      body:  Stack(children: const [BackGround(),Body()]),
    );
  }
}
