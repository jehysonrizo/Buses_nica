import 'package:buses_nica/models_departament/Matagalpa_models/Cotran_Sur/Matagalpa_leon.dart';
import 'package:flutter/material.dart';

class ItemCard extends StatefulWidget {
  final MatagalpaLeon matagalpaleon;
  // ignore: use_key_in_widget_constructors
  const ItemCard({
    required this.matagalpaleon,
  });

  @override
  State<ItemCard> createState() => _ItemCardState();
}

class _ItemCardState extends State<ItemCard> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(child: card());
  }

  Widget card() {
    return Card(
      elevation: 10.0,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
      child: Column(
        children: [
          ListTile(
            title: Text(
              widget.matagalpaleon.title,
              style: const TextStyle(fontWeight: FontWeight.w700),
            ),
            subtitle: Text(widget.matagalpaleon.subtitle),
            trailing: Text(widget.matagalpaleon.transporte,
                style: const TextStyle(fontWeight: FontWeight.w700, color: Colors.indigo)),
          ),
          const FadeInImage(
            image: AssetImage(
                'assets/bus.png'),
            placeholder: AssetImage('assets/loading.gif'),
            fadeInDuration: Duration(milliseconds: 200),
            height: 180,
            fit: BoxFit.cover,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(widget.matagalpaleon.entrada1, style: const TextStyle(fontWeight: FontWeight.w600),),
              Text(widget.matagalpaleon.entrada2, style: const TextStyle(fontWeight: FontWeight.w600),),
              Text(widget.matagalpaleon.entrada3, style: const TextStyle(fontWeight: FontWeight.w600),),
              Text(widget.matagalpaleon.entrada4, style: const TextStyle(fontWeight: FontWeight.w600),),
               const SizedBox(width: 300,),
            ],
          )
        ],
      ),
    );
  }
}
