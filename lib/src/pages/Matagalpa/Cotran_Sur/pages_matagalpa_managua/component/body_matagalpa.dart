import 'package:buses_nica/models_departament/Matagalpa_models/Cotran_Sur/Matagalpa_managua.dart';
import 'package:flutter/material.dart';
import '../../../../../../../constants.dart';
import 'matagalpa_item_card_managua.dart';



class Body extends StatefulWidget {
  const Body({Key? key}) : super(key: key);

  @override
  // ignore: library_private_types_in_public_api
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Column(
        children: [
          Expanded(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: kDefaultPaddin),
              child: GridView.builder(
                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 1,
                ),
                itemCount: matagalpamanagua.length,
                itemBuilder: (context, index) => Column(
                  children: [
                    ItemCard(
                      matagalpamanagua: matagalpamanagua[index],
                    ),
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
