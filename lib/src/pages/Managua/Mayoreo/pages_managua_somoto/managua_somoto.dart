import 'package:buses_nica/constants.dart';
import 'package:buses_nica/src/pages/Managua/Mayoreo/pages_managua_somoto/component/body_managua_somoto.dart';
import 'package:flutter/material.dart';

import '../../../../views/background.dart';

class ManaguaSomo extends StatelessWidget {
  const ManaguaSomo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backGroundColorScaff,
      appBar: AppBar(
          title: const Text('Managua - Somoto'),
          backgroundColor: backGroundColorApp,
          centerTitle: true,
          automaticallyImplyLeading: false,
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: const Icon(
              Icons.arrow_back,
              color: Colors.white,
            ),
          )),
      body: Stack(children: const [BackGround(),Body()]),
    );
  }
}
