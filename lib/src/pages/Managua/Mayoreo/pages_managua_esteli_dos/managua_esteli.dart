import 'package:buses_nica/constants.dart';
import 'package:flutter/material.dart';

import '../../../../views/background.dart';
import 'component/body_managua_esteli.dart';

class ManaguaEstel2 extends StatelessWidget {
  const ManaguaEstel2({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backGroundColorScaff,
      appBar: AppBar(
          title: const Text('Managua - Esteli'),
          backgroundColor: backGroundColorApp,
          centerTitle: true,
          automaticallyImplyLeading: false,
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: const Icon(
              Icons.arrow_back,
              color: Colors.white,
            ),
          )),
      body:  Stack(children: const [BackGround(),Body()]),
    );
  }
}
