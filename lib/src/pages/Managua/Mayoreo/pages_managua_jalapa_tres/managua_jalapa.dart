import 'package:buses_nica/constants.dart';
import 'package:flutter/material.dart';

import '../../../../views/background.dart';
import 'component/body_managua_jalapa.dart';

class ManaguaJala3 extends StatelessWidget {
  const ManaguaJala3({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backGroundColorScaff,
      appBar: AppBar(
          title: const Text('Managua - Jalapa'),
          backgroundColor: backGroundColorApp,
          centerTitle: true,
          automaticallyImplyLeading: false,
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: const Icon(
              Icons.arrow_back,
              color: Colors.white,
            ),
          )),
      body: Stack(children: const [BackGround(),Body()]),
    );
  }
}
