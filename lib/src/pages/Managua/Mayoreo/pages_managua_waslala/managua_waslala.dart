import 'package:buses_nica/constants.dart';
import 'package:buses_nica/src/views/background.dart';
import 'package:flutter/material.dart';

import 'component/body_managua_waslala.dart';

class ManaguaWas extends StatelessWidget {
  const ManaguaWas({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backGroundColorScaff,
      appBar: AppBar(
          title: const Text('Managua - Waslala'),
          backgroundColor: backGroundColorApp,
          centerTitle: true,
          automaticallyImplyLeading: false,
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: const Icon(
              Icons.arrow_back,
              color: Colors.white,
            ),
          )),
      body:  Stack(children: const [BackGround(),Body()]),
    );
  }
}
