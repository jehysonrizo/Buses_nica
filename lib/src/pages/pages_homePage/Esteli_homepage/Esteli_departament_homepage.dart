// ignore_for_file: file_names
import 'package:buses_nica/src/views/background.dart';
import 'package:flutter/material.dart';

import '../../../providers/Esteli_Provider/esteli_departament_provider.dart';

class EsteliHomePage extends StatelessWidget {
  const EsteliHomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: const Text('Esteli'),
        
          centerTitle: true,
          automaticallyImplyLeading: false,
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: const Icon(
              Icons.arrow_back,
              color: Colors.white,
            ),
          )),
      body: Stack(
        children: [const BackGround(), lista()],
      ),
    );
  }

  Widget lista() {
    return FutureBuilder(
      future: menuProviderSub.cargarDataSub(),
      initialData: const [],
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return ListView(
          children: _listaItems(snapshot.data, context),
        );
      },
    );
  }

  List<Widget> _listaItems(List<dynamic> data, BuildContext context) {
    final List<Widget> managua = [];

    for (var opt in data) {
      final widgetTemp = ListTile(
        title: Text(opt['texto']),
        textColor: Colors.white,
        leading: CircleAvatar(
          backgroundColor: Colors.blueGrey.withOpacity(0),
          child: Image.asset(
            opt['image'],
          ),
        ),
        trailing: const Icon(
          Icons.keyboard_arrow_right,
          color: Colors.white,
        ),
        onTap: () {
          Navigator.pushNamed(context, opt['ruta']);
        },
      );

      managua
        ..add(widgetTemp)
        ..add(const Divider());
    }

    return managua;
  }
}
