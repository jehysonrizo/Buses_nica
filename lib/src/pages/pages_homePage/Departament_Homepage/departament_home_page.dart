// ignore_for_file: unused_field
import 'package:buses_nica/src/About_homePage/About_HomePage.dart';
import 'package:buses_nica/src/views/background.dart';
import 'package:buses_nica/src/views/show_dialog.dart';
import 'package:flutter/material.dart';
import 'package:buses_nica/src/providers/Departament_provider/departament_provider.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: const Text('Horario de Buses Nicaragua'),
          centerTitle: true,
          actions: <Widget>[
            IconButton(
              icon: const Icon(
                Icons.info_outlined,
                color: Colors.greenAccent,
              ),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => const AboutPageHome()),
                );
              },
            )
          ]),
      body: Stack(children: [const BackGround(), departamento()]),
    );
  }

  Widget departamento() {
    return FutureBuilder(
      future: menuProvider.cargarData(),
      initialData: const [],
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return ListView(
          children: _departamentoItems(snapshot.data, context),
        );
      },
    );
  }

  List<Widget> _departamentoItems(List<dynamic> data, BuildContext context) {
    final List<Widget> dep = [];
    // ignore: unused_local_variable
    bool loading = false;

    for (var opt in data) {
      final widgetTemp = ListTile(
        title: Text(opt['texto']),
        textColor: Colors.white,
        leading: CircleAvatar(
          backgroundColor: Colors.blueGrey.withOpacity(0),
          child: Image.asset(
            opt['image'],
          ),
        ),
        trailing: const Icon(
          Icons.keyboard_arrow_right,
          color: Colors.white,
        ),
        onTap: () {
          try {
            Navigator.pushNamed(context, opt['ruta']);
          } catch (e) {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => const ShowDialog()),
            );
          }
        },
      );

      dep
        ..add(widgetTemp)
        ..add(const Divider());
    }

    return dep;
  }
}
