import 'package:buses_nica/constants.dart';
import 'package:buses_nica/src/pages/Esteli/Cotran_Norte/pages_esteli_El_Sauce/component/body_esteli_el_sauce.dart';
import 'package:flutter/material.dart';

import '../../../../views/background.dart';

class EsteliElSauce extends StatelessWidget {
  const EsteliElSauce({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backGroundColorScaff,
      appBar: AppBar(
          title: const Text('Esteli - Contran Norte'),
          backgroundColor: backGroundColorApp,
          centerTitle: true,
          automaticallyImplyLeading: false,
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: const Icon(
              Icons.arrow_back,
              color: Colors.white,
            ),
          )),
      body:  Stack(children: const [BackGround(),Body()]),
    );
  }
}
