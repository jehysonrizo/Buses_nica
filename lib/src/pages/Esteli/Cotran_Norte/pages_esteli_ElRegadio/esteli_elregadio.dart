import 'package:buses_nica/constants.dart';
import 'package:flutter/material.dart';
import '../../../../views/background.dart';
import '../pages_esteli_ElRegadio/component/body_esteli.dart';

class EsteliElReg extends StatelessWidget {
  const EsteliElReg({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backGroundColorScaff,
      appBar: AppBar(
          title: const Text('Esteli - Contran Norte'),
          backgroundColor: backGroundColorApp,
          centerTitle: true,
          automaticallyImplyLeading: false,
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: const Icon(
              Icons.arrow_back,
              color: Colors.white,
            ),
          )),
      body:  Stack(children: const [BackGround(),Body()]),
    );
  }
}
