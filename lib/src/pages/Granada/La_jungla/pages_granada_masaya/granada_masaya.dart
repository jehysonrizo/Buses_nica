import 'package:buses_nica/constants.dart';
import 'package:buses_nica/src/pages/Granada/La_jungla/pages_granada_masaya/component/body_granada.dart';
import 'package:flutter/material.dart';

import '../../../../views/background.dart';

class GranadaMasa extends StatelessWidget {
  const GranadaMasa({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backGroundColorScaff,
      appBar: AppBar(
          title: const Text('Granada - Terminal La Jungla'),
          backgroundColor: backGroundColorApp,
          centerTitle: true,
          automaticallyImplyLeading: false,
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: const Icon(
              Icons.arrow_back,
              color: Colors.white,
            ),
          )),
      body: Stack(children: const [BackGround(),Body()]),
    );
  }
}
