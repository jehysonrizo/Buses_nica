import 'package:buses_nica/models_departament/Granada_Models/La_jungla/Granada_Masaya.dart';
import 'package:flutter/material.dart';
import '../../../../../../../constants.dart';
import 'granada_item_card_masaya.dart';



class Body extends StatefulWidget {
  const Body({Key? key}) : super(key: key);

  @override
  // ignore: library_private_types_in_public_api
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Column(
        children: [
          Expanded(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: kDefaultPaddin),
              child: GridView.builder(
                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 1,
                ),
                itemCount: granadamasaya.length,
                itemBuilder: (context, index) => Column(
                  children: [
                    ItemCard(
                      granadamasaya: granadamasaya[index],
                    ),
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
