import 'package:buses_nica/constants.dart';
import 'package:buses_nica/src/pages/Granada/Coogrant/pages_granada_managua/component/body_granada.dart';
import 'package:flutter/material.dart';

import '../../../../views/background.dart';

class GranadaManaguaCoo extends StatelessWidget {
  const GranadaManaguaCoo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backGroundColorScaff,
      appBar: AppBar(
          title: const Text('Granada - Terminal Coogrant'),
          backgroundColor: backGroundColorApp,
          centerTitle: true,
          automaticallyImplyLeading: false,
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: const Icon(
              Icons.arrow_back,
              color: Colors.white,
            ),
          )),
      body:  Stack(children: const [BackGround(),Body()]),
    );
  }
}
