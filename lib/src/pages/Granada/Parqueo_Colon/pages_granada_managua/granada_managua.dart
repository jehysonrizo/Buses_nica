import 'package:buses_nica/constants.dart';
import 'package:buses_nica/src/pages/Granada/Parqueo_Colon/pages_granada_managua/component/body_granada.dart';
import 'package:flutter/material.dart';

import '../../../../views/background.dart';

class GranadaManaguaCol extends StatelessWidget {
  const GranadaManaguaCol({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backGroundColorScaff,
      appBar: AppBar(
          title: const Text('Granada - Terminal Colón'),
          backgroundColor: backGroundColorApp,
          centerTitle: true,
          automaticallyImplyLeading: false,
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: const Icon(
              Icons.arrow_back,
              color: Colors.white,
            ),
          )),
      body:  Stack(children: const [BackGround(),Body()]),
    );
  }
}
