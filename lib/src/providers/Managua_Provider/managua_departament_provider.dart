import 'package:flutter/services.dart' show rootBundle ;
import 'dart:convert';

class _MenuProvider{

  List<dynamic> managua = [];

   Future <List<dynamic>> cargarDataSub() async {

  final resp = await rootBundle.loadString('data/managua_departament_rutes.json');
    Map dataMap = json.decode(resp);
    managua = dataMap['rutas'];

    return managua;
}
}

final menuProviderSub = _MenuProvider();