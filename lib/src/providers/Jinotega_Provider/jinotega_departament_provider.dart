import 'package:flutter/services.dart' show rootBundle ;
import 'dart:convert';

class _MenuProvider{

  List<dynamic> jinotega = [];

   Future <List<dynamic>> cargarDataSub() async {

  final resp = await rootBundle.loadString('data/jinotega_departament_rutes.json');
    Map dataMap = json.decode(resp);
    jinotega = dataMap['rutas'];

    return jinotega;
}
}

final menuProviderSub = _MenuProvider();