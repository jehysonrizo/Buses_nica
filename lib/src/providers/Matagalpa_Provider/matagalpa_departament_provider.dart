import 'package:flutter/services.dart' show rootBundle ;
import 'dart:convert';

class _MenuProvider{

  List<dynamic> matagalpa = [];

   Future <List<dynamic>> cargarDataSub() async {

  final resp = await rootBundle.loadString('data/matagalpa_departament_rutes.json');
    Map dataMap = json.decode(resp);
    matagalpa = dataMap['rutas'];

    return matagalpa;
}
}

final menuProviderSub = _MenuProvider();