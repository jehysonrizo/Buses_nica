import 'package:flutter/services.dart' show rootBundle ;
import 'dart:convert';

class _MenuProvider{

  List<dynamic> granada = [];

   Future <List<dynamic>> cargarDataSub() async {

  final resp = await rootBundle.loadString('data/granada_departament_rutes.json');
    Map dataMap = json.decode(resp);
    granada = dataMap['rutas'];

    return granada;
}
}

final menuProviderSub = _MenuProvider();