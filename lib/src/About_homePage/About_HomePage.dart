// ignore_for_file: file_names

import 'package:buses_nica/src/About_homePage/body_about_page.dart';
import 'package:flutter/material.dart';

class AboutPageHome extends StatelessWidget {
  const AboutPageHome({super.key});

  @override
  Widget build(BuildContext context) {
    return const AboutPageBody();
  }
}
