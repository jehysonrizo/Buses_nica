import 'package:buses_nica/constants.dart';
import 'package:buses_nica/src/About_homePage/card_about.dart';
import 'package:flutter/material.dart';

class AboutPageBody extends StatefulWidget {
  const AboutPageBody({super.key});

  @override
  State<AboutPageBody> createState() => _AboutPageBodyState();
}

class _AboutPageBodyState extends State<AboutPageBody> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: backGroundColorApp,
        title: const Text('Acerca de Buses Nica'),
        centerTitle: true,
        leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: const Icon(
              Icons.arrow_back,
              color: Colors.white,
            ),
          )
      ),
      body: const CardAbout(),
    );
  }
}
