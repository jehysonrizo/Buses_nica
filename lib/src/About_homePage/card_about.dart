import 'package:buses_nica/src/views/background.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class CardAbout extends StatelessWidget {
 
  const CardAbout({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
          colorSchemeSeed: const Color(0xff6750a4), useMaterial3: true),
      home: Scaffold(
        body: Stack(
          children: [
            const BackGround(),
            Column(
              children: const <Widget>[
                Spacer(),
                SizedBox(child: ElevatedCard()),
                FilledCard2(),
                OutlinedCard(),
                Spacer(),
                FilledCard(),
              ],
            )
          ],
        ),
      ),
    );
  }
}

class ElevatedCard extends StatefulWidget {
  const ElevatedCard({super.key});

  @override
  State<ElevatedCard> createState() => _ElevatedCardState();
}

class _ElevatedCardState extends State<ElevatedCard> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Card(
          color: Colors.blueGrey.withOpacity(0),
          child: const CircleAvatar(
            radius: 40,
            child: Image(
              image: AssetImage(
                'assets/profile_developer.jpg',
              ),
            ),
          )),
    );
  }
}

class FilledCard extends StatelessWidget {
  const FilledCard({super.key});

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 0,
      color: Theme.of(context).colorScheme.surfaceVariant,
      child: SizedBox(
        // width: 300,
        // height: 100,
        child: Center(
          child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: const [
                  Text('Desarrollador: Jehyson Iván Rizo González'),
                  Text('Ingeniero en Sistemas'),
                  Text('App: Buses Nica'),
                ],
              )),
        ),
      ),
    );
  }
}

class FilledCard2 extends StatelessWidget {
  const FilledCard2({super.key});

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 0,
      color: Theme.of(context).colorScheme.surfaceVariant,
      child: SizedBox(
        width: 300,
        height: 100,
        child: Center(
          child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: const [
                  Text(
                      textAlign: TextAlign.start,
                      // style: TextStyle(color: Colors.redAccent),
                      'La realización de esta aplicación, es gracias a la investigación en paginas y foros cuyos datos pueden no ser los correctos en algunos departamentos.'),
                ],
              )),
        ),
      ),
    );
  }
}

class OutlinedCard extends StatelessWidget {
  const OutlinedCard({super.key});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Card(
        color: Theme.of(context).colorScheme.surfaceVariant,
        elevation: 0,
        shape: RoundedRectangleBorder(
          side: BorderSide(
            color: Theme.of(context).colorScheme.outline,
          ),
          borderRadius: const BorderRadius.all(Radius.circular(12)),
        ),
        child: SizedBox(
          width: 300,
          height: 130,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Text(
                    'Anexo mi Gmail como contacto, para corregir de ser necesario ciertos horarios, de haber un cambio en estos.'),
                ElevatedButton(
                    onPressed: () {
                      Future<void> _launchUrl() async {
                        Uri _url ='jehysonrizo@gmail.com' as Uri;
                        if (!await launchUrl(_url)) {
                          final Uri _launchUrl =
                              Uri.parse('mailto:jehysonrizo@gmail.com');
                          launchUrl(_launchUrl);
                        }
                      }
                    },
                    child: const Text('Gmail'))
              ],
            ),
          ),
        ),
      ),
    );
  }
}
