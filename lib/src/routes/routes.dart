import 'package:buses_nica/src/pages/Esteli/Cotran_Norte/pages_esteli_jinotega/esteli_jinotega.dart';
import 'package:buses_nica/src/pages/Granada/Cocibolca/pages_granada_managua/granada_managua.dart';
import 'package:buses_nica/src/pages/Granada/Coogrant/pages_granada_managua/granada_managua.dart';
import 'package:buses_nica/src/pages/Granada/La_jungla/pages_granada_masaya/granada_masaya.dart';
import 'package:buses_nica/src/pages/Granada/Parada_a_Rivas/pages_granada_rivas/granada_rivas.dart';
import 'package:buses_nica/src/pages/Granada/Parqueo_Colon/pages_granada_managua/granada_managua.dart';
import 'package:buses_nica/src/pages/Managua/Israel/pages_managua_chichigalpa/managua_chichigalpa.dart';
import 'package:buses_nica/src/pages/Managua/Israel/pages_managua_chinandega/managua_chinandega.dart';
import 'package:buses_nica/src/pages/Managua/Israel/pages_managua_leon/managua_leon.dart';
import 'package:buses_nica/src/pages/Matagalpa/Cotran_Sur/pages_matagalpa_chinandega/matagalpa_chinandega.dart';
import 'package:buses_nica/src/pages/Matagalpa/Cotran_Sur/pages_matagalpa_dario/matagalpa_dario.dart';
import 'package:buses_nica/src/pages/Matagalpa/Cotran_Sur/pages_matagalpa_esteli/matagalpa_esteli.dart';
import 'package:buses_nica/src/pages/Matagalpa/Cotran_Sur/pages_matagalpa_leon/matagalpa_leon.dart';
import 'package:buses_nica/src/pages/Matagalpa/Cotran_Sur/pages_matagalpa_masaya/matagalpa_masaya.dart';
import 'package:buses_nica/src/pages/Matagalpa/Cotran_Sur/pages_matagalpa_varios/matagalpa_varios.dart';
import 'package:buses_nica/src/pages/pages_homePage/Granada_homepage/granada_departament_home_page.dart';
import 'package:buses_nica/src/views/show_dialog.dart';
import 'package:flutter/material.dart';
import 'package:buses_nica/src/About_homePage/About_HomePage.dart';
import 'package:buses_nica/src/pages/Esteli/Cotran_Norte/pages_esteli_ElRegadio/esteli_elregadio.dart';
import 'package:buses_nica/src/pages/Esteli/Cotran_Norte/pages_esteli_El_Sauce/esteli_el_sauce.dart';
import 'package:buses_nica/src/pages/Esteli/Cotran_Norte/pages_esteli_achuapa/esteli_achuapa.dart';
import 'package:buses_nica/src/pages/Esteli/Cotran_Norte/pages_esteli_jalapa/esteli_jalapa.dart';
import 'package:buses_nica/src/pages/Esteli/Cotran_Norte/pages_esteli_la_concordia/esteli_la_concordia.dart';
import 'package:buses_nica/src/pages/Esteli/Cotran_Norte/pages_esteli_la_pita/esteli_la_pita.dart';
import 'package:buses_nica/src/pages/Esteli/Cotran_Norte/pages_esteli_managua/esteli_managua.dart';
import 'package:buses_nica/src/pages/Esteli/Cotran_Norte/pages_esteli_masaya/esteli_masaya.dart';
import 'package:buses_nica/src/pages/Esteli/Cotran_Norte/pages_esteli_ocotal/esteli_ocotal.dart';
import 'package:buses_nica/src/pages/Esteli/Cotran_Norte/pages_esteli_oro_verde/esteli_oro_verde.dart';
import 'package:buses_nica/src/pages/Esteli/Cotran_Norte/pages_esteli_pueblo_nuevo/esteli_pueblo_nuevo.dart';
import 'package:buses_nica/src/pages/Esteli/Cotran_Norte/pages_esteli_quilali/esteli_quilali.dart';
import 'package:buses_nica/src/pages/Esteli/Cotran_Norte/pages_esteli_san_marcos/esteli_san_marcos.dart';
import 'package:buses_nica/src/pages/Esteli/Cotran_Norte/pages_esteli_sanjuan_derio_coco/esteli_sanjuan_derio_coco.dart';
import 'package:buses_nica/src/pages/Esteli/Cotran_Norte/pages_esteli_sanjuandelimay/esteli_sanjuan.dart';
import 'package:buses_nica/src/pages/Esteli/Cotran_Norte/pages_esteli_sanrafael/esteli_sanrafael.dart';
import 'package:buses_nica/src/pages/Esteli/Cotran_Norte/pages_esteli_santacruz_laceiba/esteli_santa_cruz.dart';
import 'package:buses_nica/src/pages/Esteli/Cotran_Norte/pages_esteli_somoto/esteli_somoto.dart';
import 'package:buses_nica/src/pages/Esteli/Cotran_Norte/pages_esteli_wiwili/esteli_wiwili.dart';
import 'package:buses_nica/src/pages/Esteli/Cotran_Norte/pages_esteli_yali/esteli_yali.dart';
import 'package:buses_nica/src/pages/Esteli/Cotran_Sur/pages_esteli_achuapa_sur/esteli_achuapa.dart';
import 'package:buses_nica/src/pages/Esteli/Cotran_Sur/pages_esteli_el_espinal/esteli_el_espinal.dart';
import 'package:buses_nica/src/pages/Esteli/Cotran_Sur/pages_esteli_la_estanzuela/esteli_la_estanzuela.dart';
import 'package:buses_nica/src/pages/Esteli/Cotran_Sur/pages_esteli_la_trinidad/esteli_la_trinidad.dart';
import 'package:buses_nica/src/pages/Esteli/Cotran_Sur/pages_esteli_leon/esteli_leon.dart';
import 'package:buses_nica/src/pages/Esteli/Cotran_Sur/pages_esteli_managua/esteli_managua.dart';
import 'package:buses_nica/src/pages/Esteli/Cotran_Sur/pages_esteli_matagalpa/esteli_matagalpa.dart';
import 'package:buses_nica/src/pages/Esteli/Cotran_Sur/pages_esteli_murra/esteli_murra.dart';
import 'package:buses_nica/src/pages/Esteli/Cotran_Sur/pages_esteli_san_nicolas/esteli_san_nicolas.dart';
import 'package:buses_nica/src/pages/Esteli/Cotran_Sur/pages_esteli_yali_sur/esteli_yali.dart';
import 'package:buses_nica/src/pages/Jinotega/Cotran%20Norte/Contran_norte_managua/jinotega_managua.dart';
import 'package:buses_nica/src/pages/Jinotega/Cotran%20Norte/Contran_norte_matagalpa/jinotega_matagalpa.dart';
import 'package:buses_nica/src/pages/Jinotega/Cotran%20Norte/Contran_norte_pantasma/jinotega_pantasma.dart';
import 'package:buses_nica/src/pages/Jinotega/Cotran%20Norte/Contran_norte_sebaco/jinotega_sebaco.dart';
import 'package:buses_nica/src/pages/Jinotega/Cotran%20Norte/Cotran_norte_SanRafael/jinotega_sanrafael.dart';
import 'package:buses_nica/src/pages/Managua/Mayoreo/pages_managua_dalia/managua_dalia.dart';
import 'package:buses_nica/src/pages/Managua/Mayoreo/pages_managua_jalapa_dos/managua_jalapa.dart';
import 'package:buses_nica/src/pages/Managua/Mayoreo/pages_managua_jalapa_tres/managua_jalapa.dart';
import 'package:buses_nica/src/pages/Managua/Mayoreo/pages_managua_jinotega_dos/managua_jinotega.dart';
import 'package:buses_nica/src/pages/Managua/Mayoreo/pages_managua_jinotega_tres/managua_jinotega.dart';
import 'package:buses_nica/src/pages/Managua/Mayoreo/pages_managua_matagalpa_tres/managua_matagalpa.dart';
import 'package:buses_nica/src/pages/Managua/Mayoreo/pages_managua_ocotal_dos/managua_ocotal.dart';
import 'package:buses_nica/src/pages/Managua/Mayoreo/pages_managua_somoto_tres/managua_somoto.dart';
import 'package:buses_nica/src/pages/Matagalpa/Cotran_Sur/pages_matagalpa_jinotega/matagalpa_jinotega.dart';
import 'package:buses_nica/src/pages/Matagalpa/Cotran_Sur/pages_matagalpa_managua/matagalpa_managua.dart';
import 'package:buses_nica/src/pages/pages_homePage/Matagalpa_homepage/matagalpa_departament_home_page.dart';
import 'package:buses_nica/src/pages/Managua/Mayoreo/pages_managua_esteli_dos/managua_esteli.dart';
import 'package:buses_nica/src/pages/Managua/Mayoreo/pages_managua_matagalpa_dos/managua_matagalpa.dart';
import 'package:buses_nica/src/pages/Managua/Mayoreo/pages_managua_somoto_dos/managua_somoto.dart';
import 'package:buses_nica/src/pages/Jinotega/Cotran%20Norte/Contran_norte_bocay/jinotega_bocay.dart';
import 'package:buses_nica/src/pages/Jinotega/Cotran%20Norte/Contran_norte_paloblanco/jinotega_paloblanco.dart';
import 'package:buses_nica/src/pages/Jinotega/Cotran%20Norte/Cotran_norte_Esteli/jinotega_esteli.dart';
import 'package:buses_nica/src/pages/Jinotega/Cotran%20Norte/Cotran_norte_Wiwili/jinotega_wiwili.dart';
import 'package:buses_nica/src/pages/Managua/Mayoreo/pages_managua_jalapa/managua_jalapa.dart';
import 'package:buses_nica/src/pages/Managua/Mayoreo/pages_managua_jinotega/managua_jinotega.dart';
import 'package:buses_nica/src/pages/Managua/Mayoreo/pages_managua_matagalpa/managua_matagalpa.dart';
import 'package:buses_nica/src/pages/Managua/Mayoreo/pages_managua_ocotal/managua_ocotal.dart';
import 'package:buses_nica/src/pages/Managua/Mayoreo/pages_managua_somoto/managua_somoto.dart';
import 'package:buses_nica/src/pages/pages_homePage/Departament_Homepage/departament_home_page.dart';
import 'package:buses_nica/src/pages/pages_homePage/Managua_homepage/managua_departament_homepage.dart';
import 'package:buses_nica/src/pages/Jinotega/Contran%20Sur/pages_jinotega_managua/jinotega_managua.dart';
import 'package:buses_nica/src/pages/Jinotega/Contran%20Sur/pages_jinotega_matagalpa/jinotega_matagalpa.dart';
import 'package:buses_nica/src/pages/pages_homePage/Jinotega_homepage/jinotega_departament_home_page.dart';
import '../pages/Managua/Mayoreo/pages_managua_esteli/managua_esteli.dart';
import '../pages/Managua/Mayoreo/pages_managua_waslala/managua_waslala.dart';
import '../pages/pages_homePage/Esteli_homepage/Esteli_departament_homepage.dart';
import '../views/splash_screen.dart';




Map<String, WidgetBuilder> getAplicationRoutes(){
  return  <String, WidgetBuilder>{
    '/'       : (BuildContext context)=> const HomePage(),
        'splash_screen'                     : (BuildContext context)=> const SplashScreen(),
        'show_dialog'                     : (BuildContext context)=> const ShowDialog(),
        'sub_departament_jinotega'          : (BuildContext context)=> const JinotegaHomePage(),
        'sub_departament_managua'           : (BuildContext context)=> const ManaguaHomePage(),
        'sub_departament_esteli'            : (BuildContext context)=> const EsteliHomePage(),
        'sub_departament_matagalpa'         : (BuildContext context)=> const MatagalpaHomePage(),
        'sub_departament_granada'           : (BuildContext context)=> const GranadaHomePage(),
        'jinotega_matagalpa_sur'            : (BuildContext context)=> const JinotegaMata(),
        'jinotega_managua_sur'              : (BuildContext context)=> const JinotegaMana(),
        'jinotega_esteli_norte'             : (BuildContext context)=> const JinotegaEstel(),
        'jinotega_sanrafael_norte'          : (BuildContext context)=> const JinotegaSanRaf(),
        'jinotega_bocay_norte'              : (BuildContext context)=> const JinotegaBoca(),
        'jinotega_paloblanco_norte'         : (BuildContext context)=> const JinotegaPaloBlan(),
        'jinotega_wiwili_norte'             : (BuildContext context)=> const JinotegaWiwi(),
        'jinotega_pantasma_norte'           : (BuildContext context)=> const JinotegaPantas(),
        'jinotega_managua_norte'            : (BuildContext context)=> const JinotegaManaNorte(),
        'jinotega_sebaco_norte'             : (BuildContext context)=> const JinotegaSeba(),
        'jinotega_matagalpa_norte'          : (BuildContext context)=> const JinotegaMataNorte(),
        'managua_matagalpa'                 : (BuildContext context)=> const ManaguaMata(),
        'managua_esteli'                    : (BuildContext context)=> const ManaguaEstel(),
        'managua_somoto'                    : (BuildContext context)=> const ManaguaSomo(),
        'managua_ocotal'                    : (BuildContext context)=> const ManaguaOco(),
        'managua_jalapa'                    : (BuildContext context)=> const ManaguaJala(),
        'managua_jinotega'                  : (BuildContext context)=> const ManaguaJino(), 
        'managua_waslala'                   : (BuildContext context)=> const ManaguaWas(),  
        'managua_matagalpa2'                : (BuildContext context)=> const ManaguaMata2(), 
        'managua_esteli2'                   : (BuildContext context)=> const ManaguaEstel2(),
        'managua_somoto2'                   : (BuildContext context)=> const ManaguaSomo2(),
        'managua_ocotal2'                   : (BuildContext context)=> const ManaguaOco2(),
        'managua_jinotega2'                 : (BuildContext context)=> const ManaguaJino2(), 
        'managua_jalapa2'                   : (BuildContext context)=> const ManaguaJala2(),
        'managua_dalia'                     : (BuildContext context)=> const ManaguaDali(),
        'managua_matagalpa3'                : (BuildContext context)=> const ManaguaMata3(), 
        'managua_somoto3'                   : (BuildContext context)=> const ManaguaSomo3(),
        'managua_jinotega3'                 : (BuildContext context)=> const ManaguaJino3(), 
        'managua_jalapa3'                   : (BuildContext context)=> const ManaguaJala3(),
        'managua_chichigalpa_israel'        : (BuildContext context)=> const ManaguaChichi(),
        'managua_chinandega_israel'         : (BuildContext context)=> const ManaguaChina(),
        'managua_leon_israel'               : (BuildContext context)=> const ManaguaLeo(),
        'esteli_managua'                    : (BuildContext context)=> const EsteliMan(),
        'esteli_jinotega'                   : (BuildContext context)=> const EsteliJino(),
        'esteli_ocotal'                     : (BuildContext context)=> const EsteliOco(),
        'esteli_somoto'                     : (BuildContext context)=> const EsteliSomo(),
        'esteli_sanjuandelimay'             : (BuildContext context)=> const EsteliSanJua(),
        'esteli_pueblonuevo'                : (BuildContext context)=> const EsteliPuebloNue(),
        'esteli_quilali'                    : (BuildContext context)=> const EsteliQuila(),
        'esteli_yali'                       : (BuildContext context)=> const EsteliYal(),
        'esteli_sanjuanderiococo'           : (BuildContext context)=> const EsteliMSanJuanDeCoco(),
        'esteli_sanrafael'                  : (BuildContext context)=> const EsteliSanRaf(),
        'esteli_elregadio'                  : (BuildContext context)=> const EsteliElReg(),
        'esteli_wiwili'                     : (BuildContext context)=> const EsteliWiwi(),
        'esteli_elsauce'                    : (BuildContext context)=> const EsteliElSauce(),
        'esteli_achuapa'                    : (BuildContext context)=> const EsteliAchu(),
        'esteli_laconcordia'                : (BuildContext context)=> const EsteliConcordia(),
        'esteli_jalapa'                     : (BuildContext context)=> const EsteliJala(),
        'esteli_masaya'                     : (BuildContext context)=> const EsteliMasa(),
        'esteli_lapita'                     : (BuildContext context)=> const EsteliPita(),
        'esteli_santacruz'                  : (BuildContext context)=> const EsteliSanta(),
        'esteli_sanmarcos'                  : (BuildContext context)=> const EsteliMarcos(),
        'esteli_oroverde'                   : (BuildContext context)=> const EsteliOro(),
        'esteli_managuasur'                 : (BuildContext context)=> const EsteliManSur(),
        'esteli_matagalpa'                  : (BuildContext context)=> const EsteliMataSur(),
        'esteli_leon'                       : (BuildContext context)=> const EsteliLeo(),
        'esteli_latrinidad'                 : (BuildContext context)=> const EsteliLaTrinidad(),
        'esteli_sannicolas'                 : (BuildContext context)=> const EsteliSanNico(),
        'esteli_laestanzuela'               : (BuildContext context)=> const EsteliLaEstanzue(),
        'esteli_yalisur'                    : (BuildContext context)=> const EsteliYalSur(),
        'esteli_achuapasur'                 : (BuildContext context)=> const EsteliAchuSur(),
        'esteli_murra'                      : (BuildContext context)=> const EsteliMurr(),
        'esteli_elespinal'                  : (BuildContext context)=> const EsteliEspinal(),
        'matagalpa_managua'                 : (BuildContext context)=> const MatagalpaMana(),
        'matagalpa_jinotega'                : (BuildContext context)=> const MatagalpaJino(),
        'matagalpa_esteli'                  : (BuildContext context)=> const MatagalpaEstel(),
        'matagalpa_leon'                    : (BuildContext context)=> const MatagalpaLeo(),
        'matagalpa_chinandega'              : (BuildContext context)=> const MatagalpaChina(),
        'matagalpa_masaya'                  : (BuildContext context)=> const MatagalpaMasa(),
        'matagalpa_dario'                   : (BuildContext context)=> const MatagalpaDar(),
        'matagalpa_varios'                  : (BuildContext context)=> const MatagalpaVar(),
        'granada_managua_coopgrant'         : (BuildContext context)=> const GranadaManaguaCoo(),
        'granada_managua_colon'             : (BuildContext context)=> const GranadaManaguaCol(),
        'granada_managua_cocibolca'         : (BuildContext context)=> const GranadaManaguaCoci(),
        'granada_masaya'                    : (BuildContext context)=> const GranadaMasa(),
        'granada_rivas'                     : (BuildContext context)=> const GranadaRiv(),
        'about'                             : (BuildContext context)=> const AboutPageHome(),  
  };
}