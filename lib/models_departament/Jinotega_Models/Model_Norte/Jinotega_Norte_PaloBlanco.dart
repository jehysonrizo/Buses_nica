// ignore_for_file: file_names
class PaloBlanco {
  final String title,
      subtitle,
      entrada1,
      entrada2,
      entrada3,
      entrada4,
      transporte;
  final int id;
  final Duration duration;
  final Duration durationSeconds;
  PaloBlanco({
    required this.id,
    required this.subtitle,
    required this.title,
    required this.entrada1,
    required this.entrada2,
    required this.entrada3,
    required this.entrada4,
    required this.transporte,
    required this.duration,
    required this.durationSeconds,
  });
}

List<PaloBlanco> paloblanco = [
  PaloBlanco(
      id: 1,
      title: "Jinotega-PaloBlanco",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Jinotega-Palo Blanco: 4:45 am',
      entrada2: 'Salida Jinotega-Palo Blanco: 12:30 pm',
      entrada3: 'Salida Jinotega-Palo Blanco: 1:30 pm',
      entrada4: '',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
];
