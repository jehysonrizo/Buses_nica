// ignore_for_file: file_names
class JinotegaPantasma {
  final String title,
      subtitle,
      entrada1,
      entrada2,
      entrada3,
      entrada4,
      transporte;
  final int id;
  final Duration duration;
  final Duration durationSeconds;
  JinotegaPantasma({
    required this.id,
    required this.subtitle,
    required this.title,
    required this.entrada1,
    required this.entrada2,
    required this.entrada3,
    required this.entrada4,
    required this.transporte,
    required this.duration,
    required this.durationSeconds,
  });
}

List<JinotegaPantasma> pantasma = [
  JinotegaPantasma(
      id: 1,
      title: "Jinotega-Pantasma",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Jinotega-Pantasma: 4:00 am',
      entrada2: 'Salida Jinotega-Pantasma: 5:30 am',
      entrada3: 'Salida Jinotega-Pantasma: 6:30 am',
      entrada4: 'Salida Jinotega-Pantasma: 7:30 am',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Cotran Norte"),
  JinotegaPantasma(
      id: 2,
      title: "Jinotega-Pantasma",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Jinotega-Pantasma: 8:45 am',
      entrada2: 'Salida Jinotega-Pantasma: 10:00 am',
      entrada3: 'Salida Jinotega-Pantasma: 11:00 am',
      entrada4: 'Salida Jinotega-Pantasma: 12:45 pm',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Cotran Norte"),
  JinotegaPantasma(
      id: 3,
      title: "Jinotega-Pantasma",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Jinotega-Pantasma: 1:15 pm',
      entrada2: 'Salida Jinotega-Pantasma: 2:30 pm',
      entrada3: 'Salida Jinotega-Pantasma: 3:30 pm',
      entrada4: 'Salida Jinotega-Pantasma: 4:30 pm',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Cotran Norte"),
  JinotegaPantasma(
      id: 4,
      title: "Jinotega-Pantasma-Mancotal",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Jinotega-Pantasma-Mancotal: 5:30 am',
      entrada2: 'Salida Jinotega-Pantasma-Mancotal: 9:15 am',
      entrada3: '',
      entrada4: '',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Cotran Norte"),
  JinotegaPantasma(
      id: 5,
      title: "Jinotega-Pantasma-Tomayunca",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Jinotega-Pantasma-Tomayunca: 6:30 am',
      entrada2: 'Salida Jinotega-Pantasma-Tomayunca: 8:00 am',
      entrada3: 'Salida Jinotega-Pantasma-Tomayunca: 10:30 am',
      entrada4: 'Salida Jinotega-Pantasma-Tomayunca: 2:55 pm',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Cotran Norte"),
];
