// ignore_for_file: file_names
class JinotegaBocay {
  final String title,
      subtitle,
      entrada1,
      entrada2,
      entrada3,
      entrada4,
      transporte;
  final int id;
  final Duration duration;
  final Duration durationSeconds;
  JinotegaBocay({
    required this.id,
    required this.subtitle,
    required this.title,
    required this.entrada1,
    required this.entrada2,
    required this.entrada3,
    required this.entrada4,
    required this.transporte,
    required this.duration,
    required this.durationSeconds,
  });
}

List<JinotegaBocay> bocay = [
  JinotegaBocay(
      id: 1,
      title: "Jinotega-Bocay",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Jinotega-Bocay: 4:00 am',
      entrada2: 'Salida Jinotega-Bocay: 5:00 am',
      entrada3: 'Salida Jinotega-Bocay: 6:30 am',
      entrada4: 'Salida Jinotega-Bocay: 10:00 am',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
  JinotegaBocay(
      id: 2,
      title: "Jinotega-Bocay",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Jinotega-Bocay: 12:00 pm',
      entrada2: '',
      entrada3: '',
      entrada4: '',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
  JinotegaBocay(
      id: 3,
      title: "Jinotega-El Cua",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Jinotega-El Cua: 8:00 am',
      entrada2: 'Salida Jinotega-El Cua: 1:30 pm',
      entrada3: '',
      entrada4: '',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
];
