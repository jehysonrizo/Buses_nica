// ignore_for_file: file_names
class JinotegaWiwili {
  final String title,
      subtitle,
      entrada1,
      entrada2,
      entrada3,
      entrada4,
      transporte;
  final int id;
  final Duration duration;
  final Duration durationSeconds;
  JinotegaWiwili({
    required this.id,
    required this.subtitle,
    required this.title,
    required this.entrada1,
    required this.entrada2,
    required this.entrada3,
    required this.entrada4,
    required this.transporte,
    required this.duration,
    required this.durationSeconds,
  });
}

List<JinotegaWiwili> wiwili = [
  JinotegaWiwili(
      id: 1,
      title: "Jinotega-Wiwili",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Jinotega-Wiwili: 4:00 am',
      entrada2: 'Salida Jinotega-Wiwili: 6:30 am',
      entrada3: 'Salida Jinotega-La Pita-Wiwili: 7:30 am',
      entrada4: 'Salida Jinotega-Wiwili: 8:45 am',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
  JinotegaWiwili(
      id: 2,
      title: "Jinotega-Esteli",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Jinotega-Wiwili: 11:00 am',
      entrada2: 'Salida Jinotega-Wiwili: 1:15 pm',
      entrada3: '',
      entrada4: '',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
];
