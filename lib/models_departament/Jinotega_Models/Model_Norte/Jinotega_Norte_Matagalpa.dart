// ignore_for_file: file_names
class JinotegaMatagalpaNorte {
  final String title,
      subtitle,
      entrada1,
      entrada2,
      entrada3,
      entrada4,
      transporte;
  final int id;
  final Duration duration;
  final Duration durationSeconds;
  JinotegaMatagalpaNorte({
    required this.id,
    required this.subtitle,
    required this.title,
    required this.entrada1,
    required this.entrada2,
    required this.entrada3,
    required this.entrada4,
    required this.transporte,
    required this.duration,
    required this.durationSeconds,
  });
}

List<JinotegaMatagalpaNorte> jinotegamatagalpa = [
  JinotegaMatagalpaNorte(
      id: 1,
      title: "Jinotega-Matagalpa",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Jinotega-Matagalpa: 7:00 am',
      entrada2: 'Salida Jinotega-Matagalpa: 11:00 am',
      entrada3: 'Salida Jinotega-Matagalpa: 2:20 pm',
      entrada4: '',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
];
