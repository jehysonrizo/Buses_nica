// ignore_for_file: file_names
class JinotegaSebaco {
  final String title,
      subtitle,
      entrada1,
      entrada2,
      entrada3,
      entrada4,
      transporte;
  final int id;
  final Duration duration;
  final Duration durationSeconds;
  JinotegaSebaco({
    required this.id,
    required this.subtitle,
    required this.title,
    required this.entrada1,
    required this.entrada2,
    required this.entrada3,
    required this.entrada4,
    required this.transporte,
    required this.duration,
    required this.durationSeconds,
  });
}

List<JinotegaSebaco> jinotegasebaco = [
  JinotegaSebaco(
      id: 1,
      title: "Jinotega-Sebaco",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Jinotega-Sebaco: 6:20 am',
      entrada2: 'Salida Jinotega-Sebaco: 8:00 am',
      entrada3: 'Salida Jinotega-Sebaco: 12:50 pm',
      entrada4: 'Salida Jinotega-Sebaco: 3:20 pm',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
];
