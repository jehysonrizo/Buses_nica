// ignore_for_file: file_names
class JinotegaEsteli {
  final String title,
      subtitle,
      entrada1,
      entrada2,
      entrada3,
      entrada4,
      transporte;
  final int id;
  final Duration duration;
  final Duration durationSeconds;
  JinotegaEsteli({
    required this.id,
    required this.subtitle,
    required this.title,
    required this.entrada1,
    required this.entrada2,
    required this.entrada3,
    required this.entrada4,
    required this.transporte,
    required this.duration,
    required this.durationSeconds,
  });
}

List<JinotegaEsteli> esteli = [
  JinotegaEsteli(
      id: 1,
      title: "Jinotega-Esteli",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Jinotega-Esteli: 5:15 am',
      entrada2: 'Salida Jinotega-Esteli: 7:00 am',
      entrada3: 'Salida Jinotega-Esteli: 9:00 am',
      entrada4: 'Salida Jinotega-Esteli-Expreso: 10:30 am',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Cotran Norte"),
  JinotegaEsteli(
      id: 2,
      title: "Jinotega-Esteli",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Jinotega-Esteli: 1:00 pm',
      entrada2: 'Salida Jinotega-Esteli: 2:45 pm',
      entrada3: 'Salida Jinotega-Esteli: 3:30 pm',
      entrada4: '',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Cotran Norte"),
];
