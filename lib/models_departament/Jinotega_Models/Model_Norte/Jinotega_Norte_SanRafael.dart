// ignore_for_file: file_names
class JinotegaSanRafael {
  final String title,
      subtitle,
      entrada1,
      entrada2,
      entrada3,
      entrada4,
      transporte;
  final int id;
  final Duration duration;
  final Duration durationSeconds;
  JinotegaSanRafael({
    required this.id,
    required this.subtitle,
    required this.title,
    required this.entrada1,
    required this.entrada2,
    required this.entrada3,
    required this.entrada4,
    required this.transporte,
    required this.duration,
    required this.durationSeconds,
  });
}

List<JinotegaSanRafael> sanrafael = [
  JinotegaSanRafael(
      id: 1,
      title: "Jinotega-San Rafael",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Jinotega-San Rafael: 8:15 am',
      entrada2: 'Salida Jinotega-San Rafael: 9:30 am',
      entrada3: 'Salida Jinotega-San Rafael: 10:40 am',
      entrada4: 'Salida Jinotega-San Rafael: 11:45 am',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Cotran Norte"),
  JinotegaSanRafael(
      id: 2,
      title: "Jinotega-San Rafael",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Jinotega-San Rafael: 12:50 pm',
      entrada2: 'Salida Jinotega-San Rafael: 3:30 pm',
      entrada3: 'Salida Jinotega-San Rafael: 5:30 pm',
      entrada4: '',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Cotran Norte"),
];
