// ignore_for_file: file_names
class GranadaRivas {
  final String title,
      subtitle,
      entrada1,
      entrada2,
      entrada3,
      entrada4,
      transporte;
  final int id;
  final Duration duration;
  final Duration durationSeconds;
  GranadaRivas({
    required this.id,
    required this.subtitle,
    required this.title,
    required this.entrada1,
    required this.entrada2,
    required this.entrada3,
    required this.entrada4,
    required this.transporte,
    required this.duration,
    required this.durationSeconds,
  });
}

List<GranadaRivas> granadarivas = [
  GranadaRivas(
      id: 1,
      title: "Granada - Rivas",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Granada-Rivas: 5:45 am',
      entrada2: 'Salida Granada-Rivas: 9:30 am',
      entrada3: 'Salida Granada-Rivas: 11:45 am',
      entrada4: 'Salida Granada-Rivas: 1:15 pm',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
  GranadaRivas(
      id: 2,
      title: "Granada - Rivas",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Granada-Rivas: 2:45 pm',
      entrada2: '',
      entrada3: '',
      entrada4: '',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
];
