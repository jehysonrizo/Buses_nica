// ignore_for_file: file_names
class EsteliSauce {
  final String title,
      subtitle,
      entrada1,
      entrada2,
      entrada3,
      entrada4,
      transporte;
  final int id;
  final Duration duration;
  final Duration durationSeconds;
  EsteliSauce({
    required this.id,
    required this.subtitle,
    required this.title,
    required this.entrada1,
    required this.entrada2,
    required this.entrada3,
    required this.entrada4,
    required this.transporte,
    required this.duration,
    required this.durationSeconds,
  });
}

List<EsteliSauce> estelisauce = [
  EsteliSauce(
      id: 1,
      title: "Esteli - El Sauce",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Esteli-El Sauce: 6:00 am',
      entrada2: 'Salida Esteli-El Sauce: 9:15 am',
      entrada3: 'Salida Esteli-El Sauce: 12:00 pm',
      entrada4: '',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
];
