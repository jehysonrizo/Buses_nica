// ignore_for_file: file_names
class EsteliManagua {
  final String title,
      subtitle,
      entrada1,
      entrada2,
      entrada3,
      entrada4,
      transporte;
  final int id;
  final Duration duration;
  final Duration durationSeconds;
  EsteliManagua({
    required this.id,
    required this.subtitle,
    required this.title,
    required this.entrada1,
    required this.entrada2,
    required this.entrada3,
    required this.entrada4,
    required this.transporte,
    required this.duration,
    required this.durationSeconds,
  });
}

List<EsteliManagua> estelimanagua = [
  EsteliManagua(
      id: 1,
      title: "Esteli - Managua",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Esteli-Managua: 5:15 am',
      entrada2: 'Salida Esteli-Managua: 6:15 am',
      entrada3: 'Salida Esteli-Managua: 7:45 am',
      entrada4: 'Salida Esteli-Managua: 11:15 am',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
];
