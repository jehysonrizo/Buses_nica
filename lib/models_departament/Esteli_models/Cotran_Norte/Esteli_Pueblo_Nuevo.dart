// ignore_for_file: file_names
class EsteliPuebloNuevo {
  final String title,
      subtitle,
      entrada1,
      entrada2,
      entrada3,
      entrada4,
      transporte;
  final int id;
  final Duration duration;
  final Duration durationSeconds;
  EsteliPuebloNuevo({
    required this.id,
    required this.subtitle,
    required this.title,
    required this.entrada1,
    required this.entrada2,
    required this.entrada3,
    required this.entrada4,
    required this.transporte,
    required this.duration,
    required this.durationSeconds,
  });
}

List<EsteliPuebloNuevo> estelipueblonuevo = [
  EsteliPuebloNuevo(
      id: 1,
      title: "Esteli - Pueblo Nuevo",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Esteli-Pueblo Nuevo: 5:45 am',
      entrada2: 'Salida Esteli-Pueblo Nuevo: 8:45 am',
      entrada3: 'Salida Esteli-Pueblo Nuevo: 9:45 am',
      entrada4: 'Salida Esteli-Pueblo Nuevo: 10:45 am',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
  EsteliPuebloNuevo(
      id: 2,
      title: "Esteli - Pueblo Nuevo",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Esteli-Pueblo Nuevo: 11:45 am',
      entrada2: 'Salida Esteli-Pueblo Nuevo: 1:15 pm',
      entrada3: 'Salida Esteli-Pueblo Nuevo: 3:10 am',
      entrada4: 'Salida Esteli-Pueblo Nuevo: 4:50 am',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
  EsteliPuebloNuevo(
      id: 3,
      title: "Esteli - Pueblo Nuevo",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Esteli-Pueblo Nuevo: 5:50 pm',
      entrada2: '',
      entrada3: '',
      entrada4: '',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
];
