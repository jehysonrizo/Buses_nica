// ignore_for_file: file_names
class EsteliYali {
  final String title,
      subtitle,
      entrada1,
      entrada2,
      entrada3,
      entrada4,
      transporte;
  final int id;
  final Duration duration;
  final Duration durationSeconds;
  EsteliYali({
    required this.id,
    required this.subtitle,
    required this.title,
    required this.entrada1,
    required this.entrada2,
    required this.entrada3,
    required this.entrada4,
    required this.transporte,
    required this.duration,
    required this.durationSeconds,
  });
}

List<EsteliYali> esteliyali = [
  EsteliYali(
      id: 1,
      title: "Esteli - Yali",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Esteli-Yali-La Rica: 4:15 am',
      entrada2: 'Salida Esteli-Yali: 5:15 am',
      entrada3: 'Salida Esteli-Yali-pasa por MiraFlor: 6:00 am',
      entrada4: 'Salida Esteli-Yali-La Rica: 6:15 am',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
  EsteliYali(
      id: 2,
      title: "Esteli - Yali",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Esteli-Yali-pasa por SanRafael: 6:45 am',
      entrada2: 'Salida Esteli-Yali: 7:15 am',
      entrada3: 'Salida Esteli-Yali: 8:15 am',
      entrada4: 'Salida Esteli-Yali-La Rica: 9:15 am',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
  EsteliYali(
      id: 3,
      title: "Esteli - Yali",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Esteli-Yali: 11:15 am',
      entrada2: 'Salida Esteli-Yali pasa por MiraFLor: 12:00 pm',
      entrada3: 'Salida Esteli-Yali-La Rica - Las Mesas.: 12:30 pm',
      entrada4: 'Salida Esteli-Yali: 12:45 pm',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
   EsteliYali(
      id: 4,
      title: "Esteli - Yali",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Esteli-Yali-La Rica: 2:10 pm',
      entrada2: 'Salida Esteli-Yali-Pasa por MiraFlor: 3:45 pm',
      entrada3: 'Salida Esteli-Yali: 4:30 pm',
      entrada4: '',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
];
