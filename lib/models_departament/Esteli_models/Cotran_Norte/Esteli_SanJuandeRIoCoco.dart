// ignore_for_file: file_names
class EsteliSanJuanDeRioCoco {
  final String title,
      subtitle,
      entrada1,
      entrada2,
      entrada3,
      entrada4,
      transporte;
  final int id;
  final Duration duration;
  final Duration durationSeconds;
  EsteliSanJuanDeRioCoco({
    required this.id,
    required this.subtitle,
    required this.title,
    required this.entrada1,
    required this.entrada2,
    required this.entrada3,
    required this.entrada4,
    required this.transporte,
    required this.duration,
    required this.durationSeconds,
  });
}

List<EsteliSanJuanDeRioCoco> estelisanjuanderiococo = [
  EsteliSanJuanDeRioCoco(
      id: 1,
      title: "Esteli - San Juan del Río Coco",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Esteli-San Juan del Río Coco: 10:15 am',
      entrada2: 'Salida Esteli-San Juan del Río Coco: 2:30 am',
      entrada3: 'Salida Esteli-San Juan del Río Coco: 3:50 am',
      entrada4: '',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
];
