// ignore_for_file: file_names
class EsteliWiwili {
  final String title,
      subtitle,
      entrada1,
      entrada2,
      entrada3,
      entrada4,
      transporte;
  final int id;
  final Duration duration;
  final Duration durationSeconds;
  EsteliWiwili({
    required this.id,
    required this.subtitle,
    required this.title,
    required this.entrada1,
    required this.entrada2,
    required this.entrada3,
    required this.entrada4,
    required this.transporte,
    required this.duration,
    required this.durationSeconds,
  });
}

List<EsteliWiwili> esteliwiwili = [
  EsteliWiwili(
      id: 1,
      title: "Esteli - Wiwili",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Esteli-Wiwili-Transp.Mairena: 3:00 am',
      entrada2: 'Salida Esteli-Wiwili-Transp.Angel: 3:00 am',
      entrada3: 'Salida Esteli-Wiwili-Transp.Molina: 4:00 am',
      entrada4: 'Salida Esteli-Wiwili-Transp.Piton: 4:00 am',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
  EsteliWiwili(
      id: 2,
      title: "Esteli - Wiwili",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Esteli-Wiwili-Transp.Perez Zeledón: 5:00 am',
      entrada2: 'Salida Esteli-Wiwili-Transp.Rodriguez: 5:00 am',
      entrada3: 'Salida Esteli-Wiwili-Expreso.Fenix: 7:35 am',
      entrada4: 'Salida Esteli-Wiwili-Expreso.Laguna: 7:35 am',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
  EsteliWiwili(
      id: 3,
      title: "Esteli - Wiwili",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Esteli-Wiwili: 8:40 am',
      entrada2: '',
      entrada3: '',
      entrada4: '',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
];
