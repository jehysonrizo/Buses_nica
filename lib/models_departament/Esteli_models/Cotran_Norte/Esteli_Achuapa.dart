// ignore_for_file: file_names
class EsteliAchuapa {
  final String title,
      subtitle,
      entrada1,
      entrada2,
      entrada3,
      entrada4,
      transporte;
  final int id;
  final Duration duration;
  final Duration durationSeconds;
  EsteliAchuapa({
    required this.id,
    required this.subtitle,
    required this.title,
    required this.entrada1,
    required this.entrada2,
    required this.entrada3,
    required this.entrada4,
    required this.transporte,
    required this.duration,
    required this.durationSeconds,
  });
}

List<EsteliAchuapa> esteliachuapa = [
  EsteliAchuapa(
      id: 1,
      title: "Esteli - Achuapa",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Esteli-Achuapa-El Sauce: 7:00 am',
      entrada2: 'Salida Esteli-Achuapa-El Sauce: 1:00 pm',
      entrada3: 'Salida Esteli-Achuapa-El Sauce: 2:15 pm',
      entrada4: '',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
];
