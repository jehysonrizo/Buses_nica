// ignore_for_file: file_names
class EsteliSomoto {
  final String title,
      subtitle,
      entrada1,
      entrada2,
      entrada3,
      entrada4,
      transporte;
  final int id;
  final Duration duration;
  final Duration durationSeconds;
  EsteliSomoto({
    required this.id,
    required this.subtitle,
    required this.title,
    required this.entrada1,
    required this.entrada2,
    required this.entrada3,
    required this.entrada4,
    required this.transporte,
    required this.duration,
    required this.durationSeconds,
  });
}

List<EsteliSomoto> estelisomoto = [
  EsteliSomoto(
      id: 1,
      title: "Esteli - Somoto",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Esteli-Somoto: 5:30 am',
      entrada2: 'Salida Esteli-Somoto: 6:30 am',
      entrada3: 'Salida Esteli-Somoto: 7:30 am',
      entrada4: 'Salida Esteli-Somoto: 8:30 am',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
  EsteliSomoto(
      id: 2,
       title: "Esteli - Somoto",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Esteli-Somoto: 9:30 am',
      entrada2: 'Salida Esteli-Somoto: 10:30 am',
      entrada3: 'Salida Esteli-Somoto: 11:30 am',
      entrada4: 'Salida Esteli-Somoto: 12:30 pm',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
  EsteliSomoto(
      id: 3,
      title: "Esteli - Somoto",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Esteli-Somoto: 1:30 pm',
      entrada2: 'Salida Esteli-Somoto: 2:20 pm',
      entrada3: 'Salida Esteli-Somoto: 3:00 pm',
      entrada4: 'Salida Esteli-Somoto: 3:25 pm',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
  EsteliSomoto(
      id: 3,
      title: "Esteli - Somoto",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Esteli-Somoto: 4:05 pm',
      entrada2: 'Salida Esteli-Somoto: 4:40 pm',
      entrada3: 'Salida Esteli-Somoto: 5:00 pm',
      entrada4: 'Salida Esteli-Somoto: 5:20 pm',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
  EsteliSomoto(
      id: 3,
      title: "Esteli - Somoto",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Esteli-Somoto: 6:10 pm',
      entrada2: '',
      entrada3: '',
      entrada4: '',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
];
