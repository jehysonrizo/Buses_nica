// ignore_for_file: file_names
class EsteliSantaCruz {
  final String title,
      subtitle,
      entrada1,
      entrada2,
      entrada3,
      entrada4,
      transporte;
  final int id;
  final Duration duration;
  final Duration durationSeconds;
  EsteliSantaCruz({
    required this.id,
    required this.subtitle,
    required this.title,
    required this.entrada1,
    required this.entrada2,
    required this.entrada3,
    required this.entrada4,
    required this.transporte,
    required this.duration,
    required this.durationSeconds,
  });
}

List<EsteliSantaCruz> estelisantacruz = [
  EsteliSantaCruz(
      id: 1,
      title: "Esteli - Santa Cruz - La Ceiba",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Esteli-Santa Cruz - La Ceiba: 9:20 am',
      entrada2: 'Salida Esteli-Santa Cruz - La Ceiba: 12:20 pm',
      entrada3: 'Salida Esteli-Santa Cruz - La Ceiba: 3:20 pm',
      entrada4: 'Salida Esteli-Santa Cruz - La Ceiba: 5:20 pm',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
];
