// ignore_for_file: file_names
class EsteliSanJuan {
  final String title,
      subtitle,
      entrada1,
      entrada2,
      entrada3,
      entrada4,
      transporte;
  final int id;
  final Duration duration;
  final Duration durationSeconds;
  EsteliSanJuan({
    required this.id,
    required this.subtitle,
    required this.title,
    required this.entrada1,
    required this.entrada2,
    required this.entrada3,
    required this.entrada4,
    required this.transporte,
    required this.duration,
    required this.durationSeconds,
  });
}

List<EsteliSanJuan> estelisanjuan = [
  EsteliSanJuan(
      id: 1,
      title: "Esteli - San Juan de Limay",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Esteli-San Juan de Limay: 5:30 am',
      entrada2: 'Salida Esteli-San Juan de Limay: 7:00 am',
      entrada3: 'Salida Esteli-San Juan de Limay: 10:00 am',
      entrada4: 'Salida Esteli-San Juan de Limay: 12:15 pm',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
  EsteliSanJuan(
      id: 2,
      title: "Esteli - San Juan de Limay",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Esteli-San Juan de Limay: 2:00 pm',
      entrada2: 'Salida Esteli-San Juan de Limay: 3:00 pm',
      entrada3: 'Salida Esteli-San Juan de Limay: 5:05 am',
      entrada4: '',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
];
