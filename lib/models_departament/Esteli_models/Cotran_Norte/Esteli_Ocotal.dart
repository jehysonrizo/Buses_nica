// ignore_for_file: file_names
class EsteliOcotal {
  final String title,
      subtitle,
      entrada1,
      entrada2,
      entrada3,
      entrada4,
      transporte;
  final int id;
  final Duration duration;
  final Duration durationSeconds;
  EsteliOcotal({
    required this.id,
    required this.subtitle,
    required this.title,
    required this.entrada1,
    required this.entrada2,
    required this.entrada3,
    required this.entrada4,
    required this.transporte,
    required this.duration,
    required this.durationSeconds,
  });
}

List<EsteliOcotal> esteliocotal = [
  EsteliOcotal(
      id: 1,
      title: "Esteli - Ocotal",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Esteli-Ocotal: 6:00 am',
      entrada2: 'Salida Esteli-Ocotal: 7:00 am',
      entrada3: 'Salida Esteli-Ocotal: 8:00 am',
      entrada4: 'Salida Esteli-Ocotal: 9:00 am',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
  EsteliOcotal(
      id: 2,
      title: "Esteli - Ocotal",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Esteli-Ocotal: 10:00 am',
      entrada2: 'Salida Esteli-Ocotal: 11:00 am',
      entrada3: 'Salida Esteli-Ocotal: 1:00 pm',
      entrada4: 'Salida Esteli-Ocotal: 2:00 pm',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
  EsteliOcotal(
      id: 3,
      title: "Esteli - Ocotal",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Esteli-Ocotal: 2:45 pm',
      entrada2: 'Salida Esteli-Ocotal: 3:15 pm',
      entrada3: 'Salida Esteli-Ocotal: 4:20 pm',
      entrada4: 'Salida Esteli-Ocotal: 5:40 pm',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
];
