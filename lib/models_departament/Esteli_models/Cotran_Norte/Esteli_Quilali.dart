// ignore_for_file: file_names
class EsteliQuilali {
  final String title,
      subtitle,
      entrada1,
      entrada2,
      entrada3,
      entrada4,
      transporte;
  final int id;
  final Duration duration;
  final Duration durationSeconds;
  EsteliQuilali({
    required this.id,
    required this.subtitle,
    required this.title,
    required this.entrada1,
    required this.entrada2,
    required this.entrada3,
    required this.entrada4,
    required this.transporte,
    required this.duration,
    required this.durationSeconds,
  });
}

List<EsteliQuilali> esteliquilali = [
  EsteliQuilali(
      id: 1,
      title: "Esteli - Quilali",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Esteli-Quilali: 5:45 am',
      entrada2: 'Salida Esteli-Quilali: 11:05 am',
      entrada3: 'Salida Esteli-Quilali: 12:15 pm',
      entrada4: 'Salida Esteli-Quilali: 1:40 pm',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
];
