// ignore_for_file: file_names
class EsteliJalapa {
  final String title,
      subtitle,
      entrada1,
      entrada2,
      entrada3,
      entrada4,
      transporte;
  final int id;
  final Duration duration;
  final Duration durationSeconds;
  EsteliJalapa({
    required this.id,
    required this.subtitle,
    required this.title,
    required this.entrada1,
    required this.entrada2,
    required this.entrada3,
    required this.entrada4,
    required this.transporte,
    required this.duration,
    required this.durationSeconds,
  });
}

List<EsteliJalapa> estelijalapa = [
  EsteliJalapa(
      id: 1,
      title: "Esteli - Jalapa",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Esteli-Jalapa: 4:10 am',
      entrada2: 'Salida Esteli-Jalapa: 12:00 pm',
      entrada3: '',
      entrada4: '',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
];
