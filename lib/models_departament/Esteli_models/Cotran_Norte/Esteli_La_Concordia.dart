// ignore_for_file: file_names
class EsteliLaConcordia {
  final String title,
      subtitle,
      entrada1,
      entrada2,
      entrada3,
      entrada4,
      transporte;
  final int id;
  final Duration duration;
  final Duration durationSeconds;
  EsteliLaConcordia({
    required this.id,
    required this.subtitle,
    required this.title,
    required this.entrada1,
    required this.entrada2,
    required this.entrada3,
    required this.entrada4,
    required this.transporte,
    required this.duration,
    required this.durationSeconds,
  });
}

List<EsteliLaConcordia> estelilaconcordia = [
  EsteliLaConcordia(
      id: 1,
      title: "Esteli - La Concordia",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Esteli-La Concordia: 10:45 am',
      entrada2: 'Salida Esteli-La Concordia: 11:30 am',
      entrada3: 'Salida Esteli-La Concordia: 5:45 pm',
      entrada4: '',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
];
