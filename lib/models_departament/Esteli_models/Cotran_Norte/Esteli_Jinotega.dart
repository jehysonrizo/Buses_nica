// ignore_for_file: file_names
class EsteliJinotega {
  final String title,
      subtitle,
      entrada1,
      entrada2,
      entrada3,
      entrada4,
      transporte;
  final int id;
  final Duration duration;
  final Duration durationSeconds;
  EsteliJinotega({
    required this.id,
    required this.subtitle,
    required this.title,
    required this.entrada1,
    required this.entrada2,
    required this.entrada3,
    required this.entrada4,
    required this.transporte,
    required this.duration,
    required this.durationSeconds,
  });
}

List<EsteliJinotega> estelijinotega = [
  EsteliJinotega(
      id: 1,
      title: "Esteli - Jinotega",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Esteli-Jinotega: 5:45 am',
      entrada2: 'Salida Esteli-Jinotega: 8:15 am',
      entrada3: 'Salida Esteli-Jinotega: 9:15 am',
      entrada4: 'Salida Esteli-San-Rafael-Jinotega: 12:30 pm',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
  EsteliJinotega(
      id: 2,
      title: "Esteli - Jinotega",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Esteli-Jinotega: 2:15 pm',
      entrada2: 'Salida Esteli-Jinotega: 3:45 pm',
      entrada3: 'Salida Esteli-Jinotega: 4:45 pm',
      entrada4: '',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
];
