// ignore_for_file: file_names
class EsteliTrinidad {
  final String title,
      subtitle,
      entrada1,
      entrada2,
      entrada3,
      entrada4,
      transporte;
  final int id;
  final Duration duration;
  final Duration durationSeconds;
  EsteliTrinidad({
    required this.id,
    required this.subtitle,
    required this.title,
    required this.entrada1,
    required this.entrada2,
    required this.entrada3,
    required this.entrada4,
    required this.transporte,
    required this.duration,
    required this.durationSeconds,
  });
}

List<EsteliTrinidad> estelitrinidad = [
  EsteliTrinidad(
      id: 1,
      title: "Esteli - La Trinidad",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Esteli-La Trinidad: 5:15 am',
      entrada2: 'Salida Esteli-La Trinidad: 5:30 pm',
      entrada3: 'Salida Esteli-La Trinidad: 6:20 pm',
      entrada4: 'Salida Esteli-La Trinidad: 6:30 pm',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
  EsteliTrinidad(
      id: 2,
      title: "Esteli - La Trinidad",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Esteli-La Trinidad: 7:00 pm',
      entrada2: '',
      entrada3: '',
      entrada4: '',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
];
