// ignore_for_file: file_names
class EsteliManaguaSur {
  final String title,
      subtitle,
      entrada1,
      entrada2,
      entrada3,
      entrada4,
      transporte;
  final int id;
  final Duration duration;
  final Duration durationSeconds;
  EsteliManaguaSur({
    required this.id,
    required this.subtitle,
    required this.title,
    required this.entrada1,
    required this.entrada2,
    required this.entrada3,
    required this.entrada4,
    required this.transporte,
    required this.duration,
    required this.durationSeconds,
  });
}

List<EsteliManaguaSur> estelimanaguasur = [
  EsteliManaguaSur(
      id: 1,
      title: "Esteli - Managua",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Esteli-Managua: 3:30 am',
      entrada2: 'Salida Esteli-Managua: 4:00 am',
      entrada3: 'Salida Esteli-Managua: 4:30 am',
      entrada4: 'Salida Esteli-Managua-Expreso: 4:45 am',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
  EsteliManaguaSur(
      id: 2,
      title: "Esteli - Managua",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Esteli-Managua: 5:00 am',
      entrada2: 'Salida Esteli-Managua: 5:30 am',
      entrada3: 'Salida Esteli-Managua-Expreso: 5:45 am',
      entrada4: 'Salida Esteli-Managua: 6:00 am',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
  EsteliManaguaSur(
      id: 3,
      title: "Esteli - Managua",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Esteli-Managua: 6:30 am',
      entrada2: 'Salida Esteli-Managua-Expreso: 6:45 am',
      entrada3: 'Salida Esteli-Managua: 7:00 am',
      entrada4: 'Salida Esteli-Managua-Expreso: 7:15 am',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
  EsteliManaguaSur(
      id: 4,
      title: "Esteli - Managua",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Esteli-Managua: 7:30 am',
      entrada2: 'Salida Esteli-Managua: 8:00 am',
      entrada3: 'Salida Esteli-Managua: 8:30 am',
      entrada4: 'Salida Esteli-Managua-Expreso: 8:45 am',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
  EsteliManaguaSur(
      id: 5,
      title: "Esteli - Managua",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Esteli-Managua: 9:00 am',
      entrada2: 'Salida Esteli-Managua: 9:30 am',
      entrada3: 'Salida Esteli-Managua-Expreso: 9:45 am',
      entrada4: 'Salida Esteli-Managua: 10:00 am',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
  EsteliManaguaSur(
      id: 6,
      title: "Esteli - Managua",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Esteli-Managua: 10:30 am',
      entrada2: 'Salida Esteli-Managua: 11:00 am',
      entrada3: 'Salida Esteli-Managua: 11:30 am',
      entrada4: 'Salida Esteli-Managua: 12:00 pm',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
  EsteliManaguaSur(
      id: 7,
      title: "Esteli - Managua",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Esteli-Managua-Expreso: 12:15 pm',
      entrada2: 'Salida Esteli-Managua: 12:30 pm',
      entrada3: 'Salida Esteli-Managua: 1:00 pm',
      entrada4: 'Salida Esteli-Managua-Expreso: 1:15 pm',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
  EsteliManaguaSur(
      id: 8,
      title: "Esteli - Managua",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Esteli-Managua: 1:30 pm',
      entrada2: 'Salida Esteli-Managua: 2:00 pm',
      entrada3: 'Salida Esteli-Managua-Expreso: 2:15 pm',
      entrada4: 'Salida Esteli-Managua: 2:30 pm',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
  EsteliManaguaSur(
      id: 9,
      title: "Esteli - Managua",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Esteli-Managua: 3:00 pm',
      entrada2: 'Salida Esteli-Managua-Expreso: 3:15 pm',
      entrada3: 'Salida Esteli-Managua: 3:30 pm',
      entrada4: 'Salida Esteli-Managua: 4:00 pm',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
  EsteliManaguaSur(
      id: 10,
      title: "Esteli - Managua",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Esteli-Managua: 4:30 pm',
      entrada2: 'Salida Esteli-Managua: 5:00 pm',
      entrada3: 'Salida Esteli-Managua: 6:00 pm',
      entrada4: '',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
];
