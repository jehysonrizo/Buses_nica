// ignore_for_file: file_names
class EsteliLeon {
  final String title,
      subtitle,
      entrada1,
      entrada2,
      entrada3,
      entrada4,
      transporte;
  final int id;
  final Duration duration;
  final Duration durationSeconds;
  EsteliLeon({
    required this.id,
    required this.subtitle,
    required this.title,
    required this.entrada1,
    required this.entrada2,
    required this.entrada3,
    required this.entrada4,
    required this.transporte,
    required this.duration,
    required this.durationSeconds,
  });
}

List<EsteliLeon> estelileon = [
  EsteliLeon(
      id: 1,
      title: "Esteli - León",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Esteli-León: 5:00 am',
      entrada2: 'Salida Esteli-León: 5:45 am',
      entrada3: 'Salida Esteli-León: 6:45 am',
      entrada4: '',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
];
