// ignore_for_file: file_names
class EsteliSanNicolas {
  final String title,
      subtitle,
      entrada1,
      entrada2,
      entrada3,
      entrada4,
      transporte;
  final int id;
  final Duration duration;
  final Duration durationSeconds;
  EsteliSanNicolas({
    required this.id,
    required this.subtitle,
    required this.title,
    required this.entrada1,
    required this.entrada2,
    required this.entrada3,
    required this.entrada4,
    required this.transporte,
    required this.duration,
    required this.durationSeconds,
  });
}

List<EsteliSanNicolas> estelisannicolas = [
  EsteliSanNicolas(
      id: 1,
      title: "Esteli - San Nicolás",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Esteli-San Nicolás: 7:00 am',
      entrada2: 'Salida Esteli-San Nicolás: 9:00 am',
      entrada3: 'Salida Esteli-San Nicolás: 12:00 pm',
      entrada4: 'Salida Esteli-San Nicolás: 2:00 pm',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
  EsteliSanNicolas(
      id: 2,
      title: "Esteli - San Nicolás",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Esteli-San Nicolás: 3:00 pm',
      entrada2: 'Salida Esteli-San Nicolás: 4:30 pm',
      entrada3: 'Salida Esteli-San Nicolás: 6:00 pm',
      entrada4: '',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
];
