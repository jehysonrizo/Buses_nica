// ignore_for_file: file_names
class ManaguaChinandegaIsrael {
  final String title,
      subtitle,
      entrada1,
      entrada2,
      entrada3,
      entrada4,
      transporte;
  final int id;
  final Duration duration;
  final Duration durationSeconds;
  ManaguaChinandegaIsrael({
    required this.id,
    required this.subtitle,
    required this.title,
    required this.entrada1,
    required this.entrada2,
    required this.entrada3,
    required this.entrada4,
    required this.transporte,
    required this.duration,
    required this.durationSeconds,
  });
}

List<ManaguaChinandegaIsrael> managuachinandegaisrael = [
  ManaguaChinandegaIsrael(
      id: 1,
      title: "Managua - Chinandega",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Managua-Chinandega: 4:45 am',
      entrada2: 'Salida Managua-Chinandega: 5:05 am',
      entrada3: 'Salida Managua-Chinandega: 5:25 am',
      entrada4: 'Salida Managua-Chinandega: 5:45 am',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
  ManaguaChinandegaIsrael(
      id: 2,
      title: "Managua - Chinandega",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Managua-Chinandega: 6:05 am',
      entrada2: 'Salida Managua-Chinandega: 6:25 am',
      entrada3: 'Salida Managua-Chinandega: 6:45 am',
      entrada4: 'Salida Managua-Chinandega: 7:05 am',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
  ManaguaChinandegaIsrael(
      id: 3,
      title: "Managua - Chinandega",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Managua-Chinandega: 7:25 am',
      entrada2: 'Salida Managua-Chinandega: 7:45 am',
      entrada3: 'Salida Managua-Chinandega: 8:05 am',
      entrada4: 'Salida Managua-Chinandega: 8:25 am',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
  ManaguaChinandegaIsrael(
      id: 4,
      title: "Managua - Chinandega",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Managua-Chinandega: 8:45 am',
      entrada2: 'Salida Managua-Chinandega: 9:05 am',
      entrada3: 'Salida Managua-Chinandega: 9:25 am',
      entrada4: 'Salida Managua-Chinandega: 9:45 am',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
  ManaguaChinandegaIsrael(
      id: 5,
      title: "Managua - Chinandega",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Managua-Chinandega: 10:05 am',
      entrada2: 'Salida Managua-Chinandega: 10:25 am',
      entrada3: 'Salida Managua-Chinandega: 10:45 am',
      entrada4: 'Salida Managua-Chinandega: 11:05 am',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
  ManaguaChinandegaIsrael(
      id: 6,
      title: "Managua - Chinandega",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Managua-Chinandega: 11:25 am',
      entrada2: 'Salida Managua-Chinandega: 11:45 am',
      entrada3: 'Salida Managua-Chinandega: 12:05 pm',
      entrada4: 'Salida Managua-Chinandega: 12:25 pm',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
  ManaguaChinandegaIsrael(
      id: 7,
      title: "Managua - Chinandega",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Managua-Chinandega: 12:45 pm',
      entrada2: 'Salida Managua-Chinandega: 1:05 pm',
      entrada3: 'Salida Managua-Chinandega: 1:25 pm',
      entrada4: 'Salida Managua-Chinandega: 1:45 pm',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
  ManaguaChinandegaIsrael(
      id: 8,
      title: "Managua - Chinandega",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Managua-Chinandega: 2:05 pm',
      entrada2: 'Salida Managua-Chinandega: 2:25 pm',
      entrada3: 'Salida Managua-Chinandega: 2:45 pm',
      entrada4: 'Salida Managua-Chinandega: 3:05 pm',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
  ManaguaChinandegaIsrael(
      id: 9,
      title: "Managua - Chinandega",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Managua-Chinandega: 3:25 pm',
      entrada2: 'Salida Managua-Chinandega: 3:45 pm',
      entrada3: 'Salida Managua-Chinandega: 4:05 pm',
      entrada4: 'Salida Managua-Chinandega: 4:25 pm',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
  ManaguaChinandegaIsrael(
      id: 10,
      title: "Managua - Chinandega",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Managua-Chinandega: 4:45 pm',
      entrada2: 'Salida Managua-Chinandega: 5:05 pm',
      entrada3: 'Salida Managua-Chinandega: 5:25 pm',
      entrada4: 'Salida Managua-Chinandega: 5:45 pm',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
  ManaguaChinandegaIsrael(
      id: 11,
      title: "Managua - Chinandega",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Managua-Chinandega: 6:05 pm',
      entrada2: 'Salida Managua-Chinandega: 6:25 pm',
      entrada3: 'Salida Managua-Chinandega: 6:45 pm',
      entrada4: 'Salida Managua-Chinandega: 7:05 pm',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
];
