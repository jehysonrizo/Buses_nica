// ignore_for_file: file_names
class ManaguaChichigalpaIsrael {
  final String title,
      subtitle,
      entrada1,
      entrada2,
      entrada3,
      entrada4,
      transporte;
  final int id;
  final Duration duration;
  final Duration durationSeconds;
  ManaguaChichigalpaIsrael({
    required this.id,
    required this.subtitle,
    required this.title,
    required this.entrada1,
    required this.entrada2,
    required this.entrada3,
    required this.entrada4,
    required this.transporte,
    required this.duration,
    required this.durationSeconds,
  });
}

List<ManaguaChichigalpaIsrael> managuachichigalpaisrael = [
  ManaguaChichigalpaIsrael(
      id: 1,
      title: "Managua - Chichigalpa",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Managua-Chichigalpa: 9:00 am',
      entrada2: 'Salida Managua-Chichigalpa: 9:40 am',
      entrada3: 'Salida Managua-Chichigalpa: 10:20 am',
      entrada4: 'Salida Managua-Chichigalpa: 11:00 am',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
  ManaguaChichigalpaIsrael(
      id: 2,
      title: "Managua - Chichigalpa",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Managua-Chichigalpa: 11:40 am',
      entrada2: 'Salida Managua-Chichigalpa: 12:20 pm',
      entrada3: 'Salida Managua-Chichigalpa: 1:00 pm',
      entrada4: 'Salida Managua-Chichigalpa: 1:40 pm',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
  ManaguaChichigalpaIsrael(
      id: 3,
      title: "Managua - Chichigalpa",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Managua-Chichigalpa: 2:20 pm',
      entrada2: 'Salida Managua-Chichigalpa: 3:00 pm',
      entrada3: 'Salida Managua-Chichigalpa: 3:40 pm',
      entrada4: '',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
];
