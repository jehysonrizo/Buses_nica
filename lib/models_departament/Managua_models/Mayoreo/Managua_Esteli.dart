// ignore_for_file: file_names
class ManaguaEsteli {
  final String title,
      subtitle,
      entrada1,
      entrada2,
      entrada3,
      entrada4,
      transporte;
  final int id;
  final Duration duration;
  final Duration durationSeconds;
  ManaguaEsteli({
    required this.id,
    required this.subtitle,
    required this.title,
    required this.entrada1,
    required this.entrada2,
    required this.entrada3,
    required this.entrada4,
    required this.transporte,
    required this.duration,
    required this.durationSeconds,
  });
}

List<ManaguaEsteli> managuaesteli = [
  ManaguaEsteli(
      id: 1,
      title: "Managua - Esteli",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Managua-Esteli: 8:15 am',
      entrada2: 'Salida Managua-Esteli: 10:45 am',
      entrada3: 'Salida Managua-Esteli: 11:45 am',
      entrada4: 'Salida Managua-Esteli: 1:15 pm',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
  ManaguaEsteli(
      id: 2,
      title: "Managua - Esteli",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Managua-Esteli: 1:15 pm',
      entrada2: 'Salida Managua-Esteli: 1:45 pm',
      entrada3: 'Salida Managua-Esteli: 2:45 pm',
      entrada4: 'Salida Managua-Esteli: 3:15 pm',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
];
