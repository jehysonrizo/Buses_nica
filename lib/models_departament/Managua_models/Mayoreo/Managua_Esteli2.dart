// ignore_for_file: file_names
class ManaguaEsteli2 {
  final String title,
      subtitle,
      entrada1,
      entrada2,
      entrada3,
      entrada4,
      transporte;
  final int id;
  final Duration duration;
  final Duration durationSeconds;
  ManaguaEsteli2({
    required this.id,
    required this.subtitle,
    required this.title,
    required this.entrada1,
    required this.entrada2,
    required this.entrada3,
    required this.entrada4,
    required this.transporte,
    required this.duration,
    required this.durationSeconds,
  });
}

List<ManaguaEsteli2> managuaesteli2 = [
  ManaguaEsteli2(
      id: 1,
      title: "Managua - Esteli",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Managua-Esteli: 5:45 am',
      entrada2: 'Salida Managua-Esteli: 9:15 am',
      entrada3: 'Salida Managua-Esteli: 12:20 am',
      entrada4: 'Salida Managua-Esteli: 2:20 pm',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
  ManaguaEsteli2(
      id: 2,
      title: "Managua - Esteli",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Managua-Esteli: 3:50 pm',
      entrada2: '',
      entrada3: '',
      entrada4: '',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
];
