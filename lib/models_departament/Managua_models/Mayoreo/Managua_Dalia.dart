// ignore_for_file: file_names
class ManaguaDalia {
  final String title,
      subtitle,
      entrada1,
      entrada2,
      entrada3,
      entrada4,
      transporte;
  final int id;
  final Duration duration;
  final Duration durationSeconds;
  ManaguaDalia({
    required this.id,
    required this.subtitle,
    required this.title,
    required this.entrada1,
    required this.entrada2,
    required this.entrada3,
    required this.entrada4,
    required this.transporte,
    required this.duration,
    required this.durationSeconds,
  });
}

List<ManaguaDalia> managuadalia = [
  ManaguaDalia(
      id: 1,
      title: "Managua - Dalia",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Managua-Dalia: 4:30 pm',
      entrada2: '',
      entrada3: '',
      entrada4: '',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rolando Lopez"),
];
