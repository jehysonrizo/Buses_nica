// ignore_for_file: file_names
class ManaguaJinotega2 {
  final String title,
      subtitle,
      entrada1,
      entrada2,
      entrada3,
      entrada4,
      transporte;
  final int id;
  final Duration duration;
  final Duration durationSeconds;
  ManaguaJinotega2({
    required this.id,
    required this.subtitle,
    required this.title,
    required this.entrada1,
    required this.entrada2,
    required this.entrada3,
    required this.entrada4,
    required this.transporte,
    required this.duration,
    required this.durationSeconds,
  });
}

List<ManaguaJinotega2> managuajinotega2 = [
  ManaguaJinotega2(
      id: 1,
      title: "Managua - Jinotega",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Managua-Jinotega: 5:00 am',
      entrada2: 'Salida Managua-Jinotega: 8:00 am',
      entrada3: 'Salida Managua-Jinotega: 10:00 am',
      entrada4: 'Salida Managua-Jinotega: 12:00 pm',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
  ManaguaJinotega2(
      id: 2,
      title: "Managua - Jinotega",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Managua-Jinotega: 1:00 pm',
      entrada2: 'Salida Managua-Jinotega: 5:30 pm',
      entrada3: '',
      entrada4: '',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
];
