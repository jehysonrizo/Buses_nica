// ignore_for_file: file_names
class ManaguaMatagalpa {
  final String title,
      subtitle,
      entrada1,
      entrada2,
      entrada3,
      entrada4,
      transporte;
  final int id;
  final Duration duration;
  final Duration durationSeconds;
  ManaguaMatagalpa({
    required this.id,
    required this.subtitle,
    required this.title,
    required this.entrada1,
    required this.entrada2,
    required this.entrada3,
    required this.entrada4,
    required this.transporte,
    required this.duration,
    required this.durationSeconds,
  });
}

List<ManaguaMatagalpa> managuamatagalpa = [
  ManaguaMatagalpa(
      id: 1,
      title: "Managua - Matagalpa",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Managua-Matagalpa: 5:30 am',
      entrada2: 'Salida Managua-Matagalpa: 9:00 am',
      entrada3: 'Salida Managua-Matagalpa: 11:00 am',
      entrada4: 'Salida Managua-Matagalpa: 11:30 am',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
  ManaguaMatagalpa(
      id: 2,
      title: "Managua - Matagalpa",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Managua-Matagalpa: 1:30 pm',
      entrada2: 'Salida Managua-Matagalpa: 2:30 pm',
      entrada3: 'Salida Managua-Matagalpa: 3:30 pm',
      entrada4: 'Salida Managua-Matagalpa: 5:00 pm',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
];
