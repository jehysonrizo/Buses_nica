// ignore_for_file: file_names
class ManaguaOcotal {
  final String title,
      subtitle,
      entrada1,
      entrada2,
      entrada3,
      entrada4,
      transporte;
  final int id;
  final Duration duration;
  final Duration durationSeconds;
  ManaguaOcotal({
    required this.id,
    required this.subtitle,
    required this.title,
    required this.entrada1,
    required this.entrada2,
    required this.entrada3,
    required this.entrada4,
    required this.transporte,
    required this.duration,
    required this.durationSeconds,
  });
}

List<ManaguaOcotal> managuaocotal = [
  ManaguaOcotal(
      id: 1,
      title: "Managua - Ocotal",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Managua-Ocotal: 5:10 am',
      entrada2: 'Salida Managua-Ocotal: 7:45 am',
      entrada3: 'Salida Managua-Ocotal: 8:45 am',
      entrada4: 'Salida Managua-Ocotal: 11:15 am',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
  ManaguaOcotal(
      id: 2,
      title: "Managua - Ocotal",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Managua-Ocotal: 12:15 pm',
      entrada2: 'Salida Managua-Ocotal: 2:15 pm',
      entrada3: 'Salida Managua-Ocotal: 4:15 pm',
      entrada4: 'Salida Managua-Ocotal: 5:15 pm',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
];
