// ignore_for_file: file_names
class ManaguaWaslala {
  final String title,
      subtitle,
      entrada1,
      entrada2,
      entrada3,
      entrada4,
      transporte;
  final int id;
  final Duration duration;
  final Duration durationSeconds;
  ManaguaWaslala({
    required this.id,
    required this.subtitle,
    required this.title,
    required this.entrada1,
    required this.entrada2,
    required this.entrada3,
    required this.entrada4,
    required this.transporte,
    required this.duration,
    required this.durationSeconds,
  });
}

List<ManaguaWaslala> managuawaslala = [
  ManaguaWaslala(
      id: 1,
      title: "Managua - Waslala",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Managua-Waslala: 3:30 am',
      entrada2: '',
      entrada3: '',
      entrada4: '',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Sergio Lopez/Pedro Molina"),
];
