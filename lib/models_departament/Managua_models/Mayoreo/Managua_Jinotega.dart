// ignore_for_file: file_names
class ManaguaJinotega {
  final String title,
      subtitle,
      entrada1,
      entrada2,
      entrada3,
      entrada4,
      transporte;
  final int id;
  final Duration duration;
  final Duration durationSeconds;
  ManaguaJinotega({
    required this.id,
    required this.subtitle,
    required this.title,
    required this.entrada1,
    required this.entrada2,
    required this.entrada3,
    required this.entrada4,
    required this.transporte,
    required this.duration,
    required this.durationSeconds,
  });
}

List<ManaguaJinotega> managuajinotega = [
  ManaguaJinotega(
      id: 1,
      title: "Managua - Jinotega",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Managua-Jinotega: 4:00 am',
      entrada2: 'Salida Managua-Jinotega: 11:00 am',
      entrada3: 'Salida Managua-Jinotega: 12:00 am',
      entrada4: 'Salida Managua-Jinotega: 2:00 pm',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
  ManaguaJinotega(
      id: 2,
      title: "Managua - Jinotega",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Managua-Jinotega: 4:00 pm',
      entrada2: '',
      entrada3: '',
      entrada4: '',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
];
