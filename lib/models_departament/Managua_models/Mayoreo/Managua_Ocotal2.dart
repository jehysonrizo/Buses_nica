// ignore_for_file: file_names
class ManaguaOcotal2 {
  final String title,
      subtitle,
      entrada1,
      entrada2,
      entrada3,
      entrada4,
      transporte;
  final int id;
  final Duration duration;
  final Duration durationSeconds;
  ManaguaOcotal2({
    required this.id,
    required this.subtitle,
    required this.title,
    required this.entrada1,
    required this.entrada2,
    required this.entrada3,
    required this.entrada4,
    required this.transporte,
    required this.duration,
    required this.durationSeconds,
  });
}

List<ManaguaOcotal2> managuaocotal2 = [
  ManaguaOcotal2(
      id: 1,
      title: "Managua - Ocotal",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Managua-Ocotal: 6:45 am',
      entrada2: 'Salida Managua-Ocotal: 5:15 pm',
      entrada3: '',
      entrada4: '',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
];
