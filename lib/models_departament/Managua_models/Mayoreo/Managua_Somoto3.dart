// ignore_for_file: file_names
class ManaguaSomoto3 {
  final String title,
      subtitle,
      entrada1,
      entrada2,
      entrada3,
      entrada4,
      transporte;
  final int id;
  final Duration duration;
  final Duration durationSeconds;
  ManaguaSomoto3({
    required this.id,
    required this.subtitle,
    required this.title,
    required this.entrada1,
    required this.entrada2,
    required this.entrada3,
    required this.entrada4,
    required this.transporte,
    required this.duration,
    required this.durationSeconds,
  });
}

List<ManaguaSomoto3> managuasomoto3 = [
  ManaguaSomoto3(
      id: 1,
      title: "Managua - Somoto",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Managua-Somoto: 9:45 am',
      entrada2: 'Salida Managua-Somoto: 12:45 pm',
      entrada3: '',
      entrada4: '',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
];
