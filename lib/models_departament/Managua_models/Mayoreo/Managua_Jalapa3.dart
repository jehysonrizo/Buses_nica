// ignore_for_file: file_names
class ManaguaJalapa3 {
  final String title,
      subtitle,
      entrada1,
      entrada2,
      entrada3,
      entrada4,
      transporte;
  final int id;
  final Duration duration;
  final Duration durationSeconds;
  ManaguaJalapa3({
    required this.id,
    required this.subtitle,
    required this.title,
    required this.entrada1,
    required this.entrada2,
    required this.entrada3,
    required this.entrada4,
    required this.transporte,
    required this.duration,
    required this.durationSeconds,
  });
}

List<ManaguaJalapa3> managuajalapa3 = [
  ManaguaJalapa3(
      id: 1,
      title: "Managua - Jalapa",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Managua-Jalapa: 3:30 am',
      entrada2: '',
      entrada3: '',
      entrada4: '',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Chepita"),
];
