// ignore_for_file: file_names
class ManaguaSomoto2 {
  final String title,
      subtitle,
      entrada1,
      entrada2,
      entrada3,
      entrada4,
      transporte;
  final int id;
  final Duration duration;
  final Duration durationSeconds;
  ManaguaSomoto2({
    required this.id,
    required this.subtitle,
    required this.title,
    required this.entrada1,
    required this.entrada2,
    required this.entrada3,
    required this.entrada4,
    required this.transporte,
    required this.duration,
    required this.durationSeconds,
  });
}

List<ManaguaSomoto2> managuasomoto2 = [
  ManaguaSomoto2(
      id: 1,
      title: "Managua - Somoto",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Managua-Somoto: 1:40 pm',
      entrada2: 'Salida Managua-Somoto: 4:45 pm',
      entrada3: '',
      entrada4: '',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
];
