// ignore_for_file: file_names
class MatagalpaChinandega {
  final String title,
      subtitle,
      entrada1,
      entrada2,
      entrada3,
      entrada4,
      transporte;
  final int id;
  final Duration duration;
  final Duration durationSeconds;
  MatagalpaChinandega({
    required this.id,
    required this.subtitle,
    required this.title,
    required this.entrada1,
    required this.entrada2,
    required this.entrada3,
    required this.entrada4,
    required this.transporte,
    required this.duration,
    required this.durationSeconds,
  });
}

List<MatagalpaChinandega> matagalpachinandega = [
  MatagalpaChinandega(
      id: 1,
      title: "Matagalpa - Chinandega",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Matagalpa-Chinandega: 5:00 am',
      entrada2: 'Salida Matagalpa-Chinandega: 2:00 pm',
      entrada3: '',
      entrada4: '',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
];
