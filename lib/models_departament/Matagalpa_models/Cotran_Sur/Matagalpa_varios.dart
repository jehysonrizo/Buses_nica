// ignore_for_file: file_names
class MatagalpaVarios {
  final String title,
      subtitle,
      entrada1,
      entrada2,
      entrada3,
      entrada4,
      transporte;
  final int id;
  final Duration duration;
  final Duration durationSeconds;
  MatagalpaVarios({
    required this.id,
    required this.subtitle,
    required this.title,
    required this.entrada1,
    required this.entrada2,
    required this.entrada3,
    required this.entrada4,
    required this.transporte,
    required this.duration,
    required this.durationSeconds,
  });
}

List<MatagalpaVarios> matagalpavarios = [
  MatagalpaVarios(
      id: 1,
      title: "Matagalpa - La Labranza",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Matagalpa-La Labranza: 11:00 am',
      entrada2: '',
      entrada3: '',
      entrada4: '',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
  MatagalpaVarios(
      id: 2,
      title: "Matagalpa - El Cacao",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Matagalpa-El Cacao: 11:25 am',
      entrada2: '',
      entrada3: '',
      entrada4: '',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
  MatagalpaVarios(
      id: 3,
      title: "Matagalpa - Maunica",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Matagalpa-Maunica: 12:55 pm',
      entrada2: '',
      entrada3: '',
      entrada4: '',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
  MatagalpaVarios(
      id: 4,
      title: "Matagalpa - La Trinidad",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Matagalpa-La Trinidad: 6:45 pm',
      entrada2: '',
      entrada3: '',
      entrada4: '',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
  MatagalpaVarios(
      id: 3,
      title: "Matagalpa - Sebaco",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Matagalpa-Sebaco: 7:30 pm',
      entrada2: '',
      entrada3: '',
      entrada4: '',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
];
