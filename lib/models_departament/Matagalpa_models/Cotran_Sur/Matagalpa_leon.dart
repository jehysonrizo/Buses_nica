// ignore_for_file: file_names
class MatagalpaLeon {
  final String title,
      subtitle,
      entrada1,
      entrada2,
      entrada3,
      entrada4,
      transporte;
  final int id;
  final Duration duration;
  final Duration durationSeconds;
  MatagalpaLeon({
    required this.id,
    required this.subtitle,
    required this.title,
    required this.entrada1,
    required this.entrada2,
    required this.entrada3,
    required this.entrada4,
    required this.transporte,
    required this.duration,
    required this.durationSeconds,
  });
}

List<MatagalpaLeon> matagalpaleon = [
  MatagalpaLeon(
      id: 1,
      title: "Matagalpa - León",
      subtitle: 'Lunes - Sabado',
      entrada1: 'Salida Matagalpa-León: 6:00 am',
      entrada2: 'Salida Matagalpa-León: 3:00 pm',
      entrada3: 'Salida Matagalpa-León: 4:00 pm',
      entrada4: '',
      duration: const Duration(hours: 24),
      durationSeconds: const Duration(milliseconds: 60),
      transporte: "Rotativo"),
];
