import 'package:flutter/material.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';


   class AdBanner extends StatefulWidget {
  const AdBanner({super.key});

  @override
  // ignore: library_private_types_in_public_api
  _AdBannerState createState() => _AdBannerState();
}

class _AdBannerState extends State<AdBanner> {
  // Crea un anuncio de banner
  late BannerAd _bannerAd;

  // Método para cargar un anuncio de banner
  void _loadBannerAd() {
    _bannerAd.load();
  }

  // Método para descargar un anuncio de banner
  void _disposeBannerAd() {
    _bannerAd.dispose();
  }

  @override
  void initState() {
    super.initState();
    _bannerAd = BannerAd(
      size: AdSize.banner,
      adUnitId: 'ca-app-pub-3940256099942544/3419835294',
      listener: BannerAdListener(
        onAdLoaded: (Ad ad) {
          _loadBannerAd();
        },
      ),
      request: const AdRequest(),
    );
    _loadBannerAd();
  }

  @override
  void dispose() {
    _disposeBannerAd();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: _bannerAd.size.width.toDouble(),
      height: _bannerAd.size.height.toDouble(),
      child: AdWidget(ad: _bannerAd),
    );
  }
}
